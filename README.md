# 酷鲨商城运营管理平台（聚合）-服务器端

## 1. 如何运行

### 1.1. 创建数据库

你需要自行安装MySQL或MariaDB，并且，创建名为`mall_ams`和`mall_pms`的数据库：

```mysql
CREATE DATABASE mall_ams;
CREATE DATABASE mall_pms;
```

### 1.2. 创建数据表

你可以在当前项目的`/doc/sql`文件夹下找到创建数据库的脚本文件，并且，你需要：

- 在`mall_ams`数据库中执行`mall_ams.sql`脚本
- 在`mall_pms`数据库中执行`mall_pms.sql`脚本

以上2个脚本会创建本项目所需的数据表，且插入一部分测试数据。

**提示**：如果你不清楚这些测试数据的关联，最好**只使用匹配的客户端进行数据访问**，而不要随意处理这些测试数据，特别是管理员相关的数据。

### 1.3. 模块简介

在本项目中的几个子级模块项目如下：

- **csmall-commons**：通用模块，此模块项目将被其它项目依赖，本身并不独立运行

- **csmall-passport**：管理员管理模块，负责管理员管理及登录登录的模块，此模块项目将占用`9081`端口

- **csmall-product**：商品管理模块，负责品牌、类别、SPU、SKU等数据的管理，此模块项目将占用`9080`端口

- **csmall-resource**：资源模块，负责文件上传、访问静态资源，此模块项目将占用`9082`端口

以上各模块项目占用的端口将可在各模块项目下的`application-dev.yml`或相关文件中修改，但是，**请注意**：如果修改了端口号，匹配的客户端项目中也需要进行对应的调整，否则，客户端项目将无法正常访问这些后端服务。

### 1.4. 启动项目

本项目将使用Redis，请在你的计算机中安装Redis，并在启动项目之前启动它。

当你使用IntelliJ IDEA打开此项目后，点击`File`菜单，选择`Project Structure`，确保使用的JDK是`1.8`版本的。

在各模块项目中的`application-dev.yml`或相关文件名，请检查连接数据库的参数是否与你的计算机中的配置相符，至少检查：

- `spring.datasource.url`
- `spring.datasource.username`
- `spring.datasource.password`
- `spring.redis.host`
- `spring.redis.port`
- `spring.redis.username`
- `spring.redis.password`

在`csmall-resource`项目的配置文件（`application.yml`系列文件）中，请检查`csmall.upload.base-dir`属性的值，此属性将配置保存上传的图片的本机路径，请确保此路径是可用的。

以上均确认无误后，你可以分别启动`csmall-passort`、`csmall-product`、`csmall-resource`项目。

此前导入数据库脚本时，已经添加了一些默认的用户账号，当你需要登录时，可以使用这些账号，所有账号的密码都是`123456`，其中，具有最高管理权限的用户名是`root`。

当项目项目后，各项目也可以通过`/doc.html`打开API文档，以查看并调度各服务接口。