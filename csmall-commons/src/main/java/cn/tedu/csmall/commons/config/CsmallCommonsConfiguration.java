package cn.tedu.csmall.commons.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 酷鲨商城-通用模块配置类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Configuration
@ComponentScan({
        "cn.tedu.csmall.commons.ex.handler"
})
public class CsmallCommonsConfiguration {
}
