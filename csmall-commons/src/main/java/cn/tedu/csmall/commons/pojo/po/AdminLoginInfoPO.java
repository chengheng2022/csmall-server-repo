package cn.tedu.csmall.commons.pojo.po;

import lombok.Data;

import java.io.Serializable;

/**
 * 管理员登录信息的存储对象，主要用于写入到Redis中
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Data
public class AdminLoginInfoPO implements Serializable {

    /**
     * 管理员ID
     */
    private Long id;

    /**
     * 管理员的启用状态
     */
    private Integer enable;

    /**
     * 管理员登录时的IP地址
     */
    private String remoteAddr;

    /**
     * 管理员登录时的浏览器版本
     */
    private String userAgent;

    /**
     * 管理员的权限列表的JSON字符串
     */
    private String authorityListJsonString;

}
