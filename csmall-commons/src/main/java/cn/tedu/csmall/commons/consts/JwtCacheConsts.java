package cn.tedu.csmall.commons.consts;

/**
 * JWT缓存相关常量
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface JwtCacheConsts {

    /**
     * 缓存的JWT前缀
     */
    String ADMIN_JWT_PREFIX = "admin:jwt:";

}
