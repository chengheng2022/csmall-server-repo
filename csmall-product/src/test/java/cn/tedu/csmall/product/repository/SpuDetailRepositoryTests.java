package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.product.dao.persist.repository.ISpuDetailRepository;
import cn.tedu.csmall.product.pojo.entity.SpuDetail;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class SpuDetailRepositoryTests {

    @Autowired
    ISpuDetailRepository repository;

    @Test
    void insert() {
        SpuDetail spuDetail = new SpuDetail();
        spuDetail.setDetail("测试数据001");

        log.debug("插入数据之前，参数：{}", spuDetail);
        int rows = repository.insert(spuDetail);
        log.debug("插入数据完成，受影响的行数：{}", rows);
        log.debug("插入数据之后，参数：{}", spuDetail);
    }

    @Test
    void deleteById() {
        Long id = 1L;
        int rows = repository.deleteById(id);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByIds() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(5L);
        int rows = repository.deleteByIds(idList);
        log.debug("批量删除完成，受影响的行数：{}", rows);
    }

    @Test
    void update() {
        SpuDetail spuDetail = new SpuDetail();
        spuDetail.setId(1L);
        spuDetail.setDetail("新-测试数据001");

        int rows = repository.update(spuDetail);
        log.debug("更新完成，受影响的行数：{}", rows);
    }

    @Test
    void count() {
        int count = repository.count();
        log.debug("统计完成，表中的数据的数量：{}", count);
    }

}