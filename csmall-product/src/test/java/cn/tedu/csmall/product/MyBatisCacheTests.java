package cn.tedu.csmall.product;

import cn.tedu.csmall.product.dao.persist.mapper.BrandMapper;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MyBatisCacheTests {

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Test
    void cacheL1() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        BrandMapper mapper = sqlSession.getMapper(BrandMapper.class);

        System.out.println("准备执行第1次查询：id=1");
        BrandStandardVO queryObject1 = mapper.getStandardById(1L);
        System.out.println("第1次查询结束：" + queryObject1);
        System.out.println();

        System.out.println("准备执行第2次查询：id=1");
        BrandStandardVO queryObject2 = mapper.getStandardById(1L);
        System.out.println("第2次查询结束：" + queryObject2);
        System.out.println();

        System.out.println("准备执行第3次查询：id=1");
        BrandStandardVO queryObject3 = mapper.getStandardById(1L);
        System.out.println("第3次查询结束：" + queryObject3);
        System.out.println();

        System.out.println("准备执行第4次查询：id=2");
        BrandStandardVO queryObject4 = mapper.getStandardById(2L);
        System.out.println("第4次查询结束：" + queryObject4);
        System.out.println();

        // System.out.println("手动清除缓存");
        // sqlSession.clearCache();
        // System.out.println();

        // mapper.deleteById(1000000L);
        // System.out.println();

        System.out.println("准备执行第5次查询：id=2");
        BrandStandardVO queryObject5 = mapper.getStandardById(2L);
        System.out.println("第5次查询结束：" + queryObject5);
        System.out.println();

        System.out.println("准备执行第6次查询：id=1");
        BrandStandardVO queryObject6 = mapper.getStandardById(1L);
        System.out.println("第6次查询结束：" + queryObject6);
        System.out.println();
    }

    @Autowired
    BrandMapper mapper;

    @Test
    void cacheL2() {
        System.out.println("准备执行第1次查询：id=1");
        BrandStandardVO queryObject1 = mapper.getStandardById(1L);
        System.out.println("第1次查询结束：" + queryObject1);
        System.out.println();

        System.out.println("准备执行第2次查询：id=1");
        BrandStandardVO queryObject2 = mapper.getStandardById(1L);
        System.out.println("第2次查询结束：" + queryObject2);
        System.out.println();

        System.out.println("准备执行第3次查询：id=1");
        BrandStandardVO queryObject3 = mapper.getStandardById(1L);
        System.out.println("第3次查询结束：" + queryObject3);
        System.out.println();

        System.out.println("准备执行第4次查询：id=2");
        BrandStandardVO queryObject4 = mapper.getStandardById(2L);
        System.out.println("第4次查询结束：" + queryObject4);
        System.out.println();

        // mapper.deleteById(1000000L);
        // System.out.println();

        System.out.println("准备执行第5次查询：id=2");
        BrandStandardVO queryObject5 = mapper.getStandardById(2L);
        System.out.println("第5次查询结束：" + queryObject5);
        System.out.println();

        System.out.println("准备执行第6次查询：id=1");
        BrandStandardVO queryObject6 = mapper.getStandardById(1L);
        System.out.println("第6次查询结束：" + queryObject6);
        System.out.println();
    }

}