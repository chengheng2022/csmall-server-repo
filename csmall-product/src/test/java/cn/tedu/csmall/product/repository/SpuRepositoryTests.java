package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.entity.Spu;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class SpuRepositoryTests {

    @Autowired
    ISpuRepository repository;

    @Transactional // 添加此注解，可使得插入的数据不保存，以避免不修改测试数据时反复执行导致的主键冲突错误
    @Test
    void insert() {
        Spu spu = new Spu();
        spu.setId(1L);
        spu.setTitle("测试数据001");

        log.debug("插入数据之前，参数：{}", spu);
        int rows = repository.insert(spu);
        log.debug("插入数据完成，受影响的行数：{}", rows);
        log.debug("插入数据之后，参数：{}", spu);
    }

    @Transactional // 添加此注解，可使得插入的数据不保存，以避免不修改测试数据时反复执行导致的主键冲突错误
    @Test
    void insertBatch() {
        List<Spu> spuList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Spu spu = new Spu();
            spu.setId(i + 0L);
            spu.setTitle("批量插入测试数据" + i);
            spuList.add(spu);
        }

        int rows = repository.insertBatch(spuList);
        log.debug("批量插入完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteById() {
        Long id = 1L;
        int rows = repository.deleteById(id);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByIds() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(5L);
        int rows = repository.deleteByIds(idList);
        log.debug("批量删除完成，受影响的行数：{}", rows);
    }

    @Test
    void update() {
        Spu spu = new Spu();
        spu.setId(1L);
        spu.setTitle("新-测试数据001");

        int rows = repository.update(spu);
        log.debug("更新完成，受影响的行数：{}", rows);
    }

    @Test
    void count() {
        int count = repository.count();
        log.debug("统计完成，表中的数据的数量：{}", count);
    }

    @Test
    void countByAlbum() {
        Long albumId = 1L;
        int countByBrand = repository.countByAlbum(albumId);
        log.debug("根据相册【{}】统计完成，数量：{}", albumId, countByBrand);
    }

    @Test
    void countByBrand() {
        Long brandId = 1L;
        int countByBrand = repository.countByBrand(brandId);
        log.debug("根据品牌【{}】统计完成，数量：{}", brandId, countByBrand);
    }

    @Test
    void countByCategory() {
        Long categoryId = 1L;
        int count = repository.countByCategory(categoryId);
        log.debug("根据类别【{}】统计关联数据的数量：{}", categoryId, count);
    }

    @Test
    void countByAttributeTemplate() {
        Long attributeTemplateId = 1L;
        int count = repository.countByAttributeTemplate(attributeTemplateId);
        log.debug("根据属性模板【{}】统计关联数据的数量：{}", attributeTemplateId, count);
    }

    @Test
    void getStandardById() {
        Long id = 1L;
        Object queryResult = repository.getStandardById(id);
        log.debug("根据id【{}】查询数据详情完成，查询结果：{}", id, queryResult);
    }

    @Test
    void list() {
        Integer pageNum = 1;
        Integer pageSize = 5;
        PageData<?> pageData = repository.list(pageNum, pageSize);
        List<?> list = pageData.getList();
        log.debug("查询列表完成，结果：{}", pageData);
        for (Object item : list) {
            log.debug("列表项：{}", item);
        }
    }

}