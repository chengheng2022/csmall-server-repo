package cn.tedu.csmall.product;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class SetTests {

    @Test
    void test() {
        Set<String> strings = new HashSet<>();
        strings.add("str-5");
        strings.add("str-4");
        strings.add("str-1");
        strings.add("str-3");
        strings.add("str-2");

        for (String string : strings) {
            System.out.println(string);
        }
    }
}
