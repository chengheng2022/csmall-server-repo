package cn.tedu.csmall.product.service;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.param.AlbumAddNewParam;
import cn.tedu.csmall.product.pojo.param.AlbumUpdateInfoParam;
import cn.tedu.csmall.product.pojo.vo.AlbumStandardVO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
public class AlbumServiceTests {

    @Autowired
    IAlbumService service; // 不建议声明为实现类类型

    @Test
    void addNew() {
        AlbumAddNewParam albumAddNewParam = new AlbumAddNewParam();
        albumAddNewParam.setName("测试名称203");
        albumAddNewParam.setDescription("测试简介101");
        albumAddNewParam.setSort(101); // 注意：由于MySQL中表设计的限制，此值只能是[0,255]区间内的

        try {
            service.addNew(albumAddNewParam);
            log.debug("添加相册成功！");
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }

    @Test
    void delete() {
        Long id = 1L;

        try {
            service.delete(id);
            log.debug("删除相册成功！");
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }

    @Test
    void updateInfoById() {
        Long id = 1L;

        AlbumUpdateInfoParam albumUpdateInfoParam = new AlbumUpdateInfoParam();
        albumUpdateInfoParam.setName("华为Mate10的相册");
        albumUpdateInfoParam.setDescription("华为Mate10的相册的简介");
        albumUpdateInfoParam.setSort(199);

        try {
            service.updateInfoById(id, albumUpdateInfoParam);
            log.debug("修改相册详情成功！");
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }

    @Test
    void getStandardById() {
        Long id = 10L;

        try {
            AlbumStandardVO album = service.getStandardById(id);
            log.debug("查询相册详情成功！结果：{}", album);
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }

    @Test
    void list() {
        try {
            Integer pageNum = 1;
            Integer pageSize = 5;
            PageData<?> pageData = service.list(pageNum, pageSize);
            List<?> list = pageData.getList();
            log.debug("查询列表完成，结果：{}", pageData);
            for (Object item : list) {
                log.debug("列表项：{}", item);
            }
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }

}
