package cn.tedu.csmall.product;

import cn.tedu.csmall.product.pojo.entity.Album;

public class ExceptionTests {

    void a() throws Exception {
        throw new Exception();
    }

    void b() {
        try {
            a();
        } catch (Exception e) {
        }
    }

    void c() throws Exception {
        a();
    }

    // ====================================

    void x() {
        throw new RuntimeException();
    }

    void y() {
        x();
    }

    // ====================================

    void npe() {
        String name = new Album().getName();
        if (name != null) {
            System.out.println("name的长度：" + name.length());
        } else {
            System.out.println("name无有效值，无法判断长度！");
        }
    }

    void handler() {
        try {
            // controller.delete();
        } catch (Exception e) {
            // 统一处理
        }
    }

}
