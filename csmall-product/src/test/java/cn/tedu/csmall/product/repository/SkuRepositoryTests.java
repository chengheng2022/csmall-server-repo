package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.dao.persist.repository.ISkuRepository;
import cn.tedu.csmall.product.pojo.entity.Sku;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class SkuRepositoryTests {

    @Autowired
    ISkuRepository repository;

    @Transactional // 添加此注解，可使得插入的数据不保存，以避免不修改测试数据时反复执行导致的主键冲突错误
    @Test
    void insert() {
        Sku sku = new Sku();
        sku.setId(1L);
        sku.setTitle("测试数据001");

        log.debug("插入数据之前，参数：{}", sku);
        int rows = repository.insert(sku);
        log.debug("插入数据完成，受影响的行数：{}", rows);
        log.debug("插入数据之后，参数：{}", sku);
    }

    @Transactional // 添加此注解，可使得插入的数据不保存，以避免不修改测试数据时反复执行导致的主键冲突错误
    @Test
    void insertBatch() {
        List<Sku> skuList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Sku sku = new Sku();
            sku.setId(i + 0L);
            sku.setTitle("批量插入测试数据" + i);
            skuList.add(sku);
        }

        int rows = repository.insertBatch(skuList);
        log.debug("批量插入完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteById() {
        Long id = 1L;
        int rows = repository.deleteById(id);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByIds() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(5L);
        int rows = repository.deleteByIds(idList);
        log.debug("批量删除完成，受影响的行数：{}", rows);
    }

    @Test
    void update() {
        Sku sku = new Sku();
        sku.setId(1L);
        sku.setTitle("新-测试数据001");

        int rows = repository.update(sku);
        log.debug("更新完成，受影响的行数：{}", rows);
    }

    @Test
    void count() {
        int count = repository.count();
        log.debug("统计完成，表中的数据的数量：{}", count);
    }

    @Test
    void countByAlbumId() {
        Long albumId = 1L;
        int count = repository.countByAlbum(albumId);
        log.debug("统计完成，根据相册【{}】统计SKU的数量，结果：{}", albumId, count);
    }

    @Test
    void getStandardById() {
        Long id = 1L;
        Object queryResult = repository.getStandardById(id);
        log.debug("根据id【{}】查询数据详情完成，查询结果：{}", id, queryResult);
    }

    @Test
    void listBySpuId() {
        Long spuId = 1L;
        Integer pageNum = 1;
        Integer pageSize = 5;
        PageData<?> pageData = repository.listBySpuId(spuId, pageNum, pageSize);
        List<?> list = pageData.getList();
        log.debug("查询列表完成，结果：{}", pageData);
        for (Object item : list) {
            log.debug("列表项：{}", item);
        }
    }

}