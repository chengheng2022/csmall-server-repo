package cn.tedu.csmall.product.service;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.product.pojo.param.SpuAddNewParam;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
public class SpuServiceTests {

    @Autowired
    ISpuService service;

    @Test
    void addNew() {
        SpuAddNewParam spuAddNewParam = new SpuAddNewParam();
        spuAddNewParam.setBrandId(2L);
        spuAddNewParam.setCategoryId(71L);
        spuAddNewParam.setAlbumId(1L);
        spuAddNewParam.setName("Test-002");

        try {
            service.addNew(spuAddNewParam);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
    }

}
