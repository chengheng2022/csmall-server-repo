package cn.tedu.csmall.product.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.dao.persist.repository.IAttributeRepository;
import cn.tedu.csmall.product.pojo.entity.Attribute;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class AttributeRepositoryTests {

    @Autowired
    IAttributeRepository repository;

    @Test
    void insert() {
        Attribute attribute = new Attribute();
        attribute.setName("测试数据001");
        attribute.setSort(255);

        log.debug("插入数据之前，参数：{}", attribute);
        int rows = repository.insert(attribute);
        log.debug("插入数据完成，受影响的行数：{}", rows);
        log.debug("插入数据之后，参数：{}", attribute);
    }

    @Test
    void insertBatch() {
        List<Attribute> attributes = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Attribute attribute = new Attribute();
            attribute.setName("批量插入测试数据" + i);
            attribute.setSort(200);
            attributes.add(attribute);
        }

        int rows = repository.insertBatch(attributes);
        log.debug("批量插入完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteById() {
        Long id = 1L;
        int rows = repository.deleteById(id);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByIds() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(5L);
        int rows = repository.deleteByIds(idList);
        log.debug("批量删除完成，受影响的行数：{}", rows);
    }

    @Test
    void update() {
        Attribute attribute = new Attribute();
        attribute.setId(1L);
        attribute.setName("新-测试数据001");

        int rows = repository.update(attribute);
        log.debug("更新完成，受影响的行数：{}", rows);
    }

    @Test
    void count() {
        int count = repository.count();
        log.debug("统计完成，表中的数据的数量：{}", count);
    }

    @Test
    void countByTemplateId() {
        Long templateId = 1L;
        int count = repository.countByTemplate(templateId);
        log.debug("根据属性模板【{}】统计属性数量完成，统计结果：{}", templateId, count);
    }

    @Test
    void countByNameAndTemplate() {
        String name = "小米15的颜色属性";
        Long templateId = 1L;
        int count = repository.countByNameAndTemplate(name, templateId);
        log.debug("根据名称【{}】在属性模板【{}】中统计属性数量完成，统计结果：{}", name, templateId, count);
    }

    @Test
    void countByNameAndTemplateAndNotId() {
        Long id = 1L;
        String name = "小米15的颜色属性";
        Long templateId = 1L;
        int count = repository.countByNameAndTemplateAndNotId(id, name, templateId);
        log.debug("根据名称【{}】且属性模板ID【{}】且非ID【{}】统计数量完成，统计结果：{}", name, templateId, id, count);
    }

    @Test
    void getStandardById() {
        Long id = 1L;
        Object queryResult = repository.getStandardById(id);
        log.debug("根据id【{}】查询数据详情完成，查询结果：{}", id, queryResult);
    }

    @Test
    void listByTemplateId() {
        Long templateId = 1L;
        Integer pageNum = 1;
        Integer pageSize = 5;
        PageData<?> pageData = repository.listByTemplateId(templateId, pageNum, pageSize);
        List<?> list = pageData.getList();
        log.debug("查询列表完成，结果：{}", pageData);
        for (Object item : list) {
            log.debug("列表项：{}", item);
        }
    }

}
