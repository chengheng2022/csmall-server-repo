package cn.tedu.csmall.product;

import cn.tedu.csmall.product.pojo.entity.Album;
import cn.tedu.csmall.product.pojo.entity.Brand;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * RedisTemplate的API演示
 */
@Slf4j
@SpringBootTest
public class RedisTest {

    // 提示：当写入的数据包含“非ASCII码字符”时，在终端窗口中无法正常显示，是正常现象，也并不影响后续读取数据
    // opsForValue()：返回ValueOperations对象，只要是对Redis中的string进行操作，都需要此类型对象的API
    // opsForList()：返回ListOperations对象，只要是对Redis中的list进行操作，都需要此类型对象的API
    // opsForSet()：返回SetOperations对象，只要是对Redis中的set进行操作，都需要此类型对象的API
    @Autowired
    RedisTemplate<String, Serializable> redisTemplate;

    // 存入字符串类型的值
    @Test
    void set() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        opsForValue.set("spu_id", 1);
        log.debug("向Redis中存入Value类型（string）的数据，成功！");
    }

    // 读取字符串类型的值
    @Test
    void get() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        String key = "username";
        Serializable value = opsForValue.get(key);
        log.debug("从Redis中读取Value类型（string）数据，Key={}，Value={}", key, value);
    }

    // 可以存入对象
    @Test
    void setObject() {
        Brand brand = new Brand();
        brand.setId(1L);
        brand.setName("大米");

        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        opsForValue.set("brand1", brand);
        log.debug("向Redis中存入Value类型（string）的数据，成功！");
    }

    // 读取到的对象的类型就是此前存入时的类型
    @Test
    void getObject() {
        try {
            ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
            String key = "brand1";
            Serializable value = opsForValue.get(key);
            log.debug("从Redis中读取Value类型（string）数据，完成！");
            log.debug("Key={}，Value={}", key, value);

            log.debug("Value的类型：{}", value.getClass().getName());
            Brand brand = (Brand) value;
            log.debug("可以将Value转换回此前存入时的类型！");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    // 如果Key并不存在，则读取的结果为null
    @Test
    void getEmpty() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        String key = "EmptyKey";
        Serializable value = opsForValue.get(key);
        log.debug("从Redis中读取Value类型（string）数据，Key={}，Value={}", key, value);
    }

    // 使用keys命令对应的API
    @Test
    void keys() {
        String pattern = "username*";
        Set<String> keys = redisTemplate.keys(pattern); // keys *
        log.debug("根据模式【{}】查询Key，结果：{}", pattern, keys);
    }

    // 删除指定的Key的数据
    @Test
    void delete() {
        String key = "age";
        Boolean result = redisTemplate.delete(key);
        log.debug("根据Key【{}】删除数据，结果：{}", key, result);
    }

    // 批量删除指定的Key的数据，与删除某1个数据的方法相同，只是参数列表不同
    @Test
    void deleteBatch() {
        Set<String> keys = new HashSet<>();
        keys.add("username");
        keys.add("username1");
        keys.add("username2");

        Long count = redisTemplate.delete(keys);
        log.debug("根据Key【{}】批量删除数据，删除的数据的数量：{}", keys, count);
    }

    // 向Redis中存入List类型的数据
    // 注意：反复执行相同的代码，会使得同一个List中有多份同样的数据
    @Test
    void rightPush() {
        List<Album> albumList = new ArrayList<>();
        for (int i = 1; i <= 8; i++) {
            Album album = new Album();
            album.setId(i + 0L);
            album.setName("测试相册-" + i);
            albumList.add(album);
        }

        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        String key = "album:lists";
        for (Album album : albumList) {
            opsForList.rightPush(key, album);
        }
        log.debug("向Redis中写入List类型的数据，完成！");
    }

    // 从Redis中取出List类型的数据列表
    @Test
    void range() {
        String key = "albums";
        long start = 0L;
        long end = -1L;

        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        List<Serializable> serializableList = opsForList.range(key, start, end);
        log.debug("从Redis中读取Key【{}】的List数据，数据量：{}", key, serializableList.size());
        for (Serializable serializable : serializableList) {
            log.debug("{}", serializable);
        }
    }

    // 从Redis中读取List的长度
    @Test
    void size() {
        String key = "albums";

        ListOperations<String, Serializable> opsForList = redisTemplate.opsForList();
        Long size = opsForList.size(key);
        log.debug("从Redis中读取Key【{}】的List的长度，结果：{}", key, size);
    }

    // 向Redis中存入Set类型的数据
    // Set中的元素必须是唯一的，如果反复添加，后续的添加并不会成功
    @Test
    void add() {
        String key = "albumItemKeys";

        SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
        Long add = opsForSet.add(key, "album:item:100");
        log.debug("向Redis中存入Set类型的数据，结果：{}", add);
    }

    // 向Redis中批量存入Set类型的数据
    @Test
    void addBatch() {
        String key = "brandItemKeys";

        SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
        Long add = opsForSet.add(key, "brand:item:1", "brand:item:2", "brand:item:3");
        log.debug("向Redis中存入Set类型的数据，结果：{}", add);
    }

    // 从Redis中取出Set类型的数据集合
    @Test
    void members() {
        String key = "albumItemKeys";

        SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
        Set<Serializable> members = opsForSet.members(key);
        log.debug("从Redis中读取Key【{}】的Set数据，数据量：{}", key, members.size());
        for (Serializable serializable : members) {
            log.debug("{}", serializable);
        }
    }

    // 从Redis中读取Set的长度
    @Test
    void sizeSet() {
        String key = "albumItemKeys";

        SetOperations<String, Serializable> opsForSet = redisTemplate.opsForSet();
        Long size = opsForSet.size(key);
        log.debug("从Redis中读取Key【{}】的Set的长度，结果：{}", key, size);
    }

    @Test
    void increment() {
        Long spuId = redisTemplate.opsForValue().increment("spu_id");
        System.out.println(spuId);
    }

}
