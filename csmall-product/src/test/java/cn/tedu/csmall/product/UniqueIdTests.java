package cn.tedu.csmall.product;

import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.UUID;

public class UniqueIdTests {

    @Test
    void uuid() {
        for (int i = 0; i < 10; i++) {
            String uuid = UUID.randomUUID().toString();
            System.out.println(uuid);
        }
    }

    @Test
    void timeMillis() {
        for (int i = 0; i < 10; i++) {
            System.out.println(System.currentTimeMillis());
        }
    }

    @Test
    void nanoTime() {
        long[] nanoTimes = new long[50];
        for (int i = 0; i < 50; i++) {
            nanoTimes[i] = System.nanoTime();
        }
        for (long nanoTime : nanoTimes) {
            System.out.println(nanoTime);
        }
    }

    @Test
    void test() {
        Random random = new Random();
        long[] nanoTimes = new long[50];
        for (int i = 0; i < 50; i++) {
            nanoTimes[i] = System.nanoTime() * 100 + (random.nextInt(8999) + 1000);
        }
        for (long nanoTime : nanoTimes) {
            System.out.println(nanoTime);
        }
    }

//    @Test
//    void getIncrementId() {
//        for (int i = 0; i < 20; i++) {
//            System.out.println(IdUtils.generateId());
//        }
//    }

}
