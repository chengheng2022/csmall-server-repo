package cn.tedu.csmall.product.service.impl;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.web.ServiceCode;
import cn.tedu.csmall.product.dao.persist.repository.ISkuRepository;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.entity.Sku;
import cn.tedu.csmall.product.pojo.param.SkuAddNewParam;
import cn.tedu.csmall.product.pojo.vo.SkuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SkuStandardVO;
import cn.tedu.csmall.product.pojo.vo.SpuStandardVO;
import cn.tedu.csmall.product.service.ISkuService;
import cn.tedu.csmall.product.util.IdUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 处理SKU的业务实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Service
public class SkuServiceImpl implements ISkuService {

    @Autowired
    private IdUtils idUtils;
    @Autowired
    private ISpuRepository spuRepository;
    @Autowired
    private ISkuRepository skuRepository;

    public SkuServiceImpl() {
        log.info("创建业务对象：SkuServiceImpl");
    }

    @Override
    public void addNew(SkuAddNewParam skuAddNewParam) {
        log.debug("开始处理【新增SKU】的业务，参数：{}", skuAddNewParam);
        // 检查品牌：是否存在，是否启用
        Long spuId = skuAddNewParam.getSpuId();
        SpuStandardVO spu = spuRepository.getStandardById(spuId);
        if (spu == null) {
            String message = "新增SKU失败，SPU数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 获取SKU ID
        Long skuId = idUtils.generateSkuId();

        // 创建SKu对象
        Sku sku = new Sku();
        // 复制属性值
        BeanUtils.copyProperties(skuAddNewParam, sku);
        // 补全属性值：id >>> 暂时随便写
        sku.setId(skuId);
        // 补全属性值
        sku.setAttributeTemplateId(spu.getAttributeTemplateId());
        sku.setAlbumId(spu.getAlbumId());
        sku.setPictures(spu.getPictures());
        sku.setSales(0);
        sku.setCommentCount(0);
        sku.setPositiveCommentCount(0);
        // 插入SKu数据
        int rows = skuRepository.insert(sku);
        if (rows != 1) {
            String message = "新增SKU失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public SkuStandardVO getStandardById(Long id) {
        log.debug("开始处理【根据ID查询SPU详情】的业务，参数：{}", id);
        SkuStandardVO queryResult = skuRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "根据ID查询SPU详情失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        log.debug("即将返回SPU详情：{}", queryResult);
        return queryResult;
    }

    @Override
    public PageData<SkuListItemVO> listBySpuId(Long spuId) {
        log.debug("开始处理【根据SPU查询SKU列表】的业务，SPU：{}]", spuId);
        Integer pageNum = 1;
        PageData<SkuListItemVO> pageData = skuRepository.listBySpuId(spuId, pageNum, Integer.MAX_VALUE);
        return pageData;
    }

}
