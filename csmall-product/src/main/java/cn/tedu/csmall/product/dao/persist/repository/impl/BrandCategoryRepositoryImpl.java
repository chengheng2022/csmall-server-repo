package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.BrandCategoryMapper;
import cn.tedu.csmall.product.dao.persist.repository.IBrandCategoryRepository;
import cn.tedu.csmall.product.pojo.entity.BrandCategory;
import cn.tedu.csmall.product.pojo.vo.BrandCategoryListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandCategoryStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理品牌与类别的关联数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class BrandCategoryRepositoryImpl implements IBrandCategoryRepository {

    @Autowired
    private BrandCategoryMapper brandCategoryMapper;

    public BrandCategoryRepositoryImpl() {
        log.debug("创建数据访问实现类对象：BrandCategoryRepositoryImpl");
    }

    @Override
    public int insert(BrandCategory brandCategory) {
        log.debug("开始执行【插入品牌与类别的关联】的数据访问，参数：{}", brandCategory);
        return brandCategoryMapper.insert(brandCategory);
    }

    @Override
    public int insertBatch(List<BrandCategory> brandCategoryList) {
        log.debug("开始执行【批量插入品牌与类别的关联】的数据访问，参数：{}", brandCategoryList);
        return brandCategoryMapper.insertBatch(brandCategoryList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除品牌与类别的关联】的数据访问，参数：{}", id);
        return brandCategoryMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除品牌与类别的关联】的数据访问，参数：{}", idList);
        return brandCategoryMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(BrandCategory brandCategory) {
        log.debug("开始执行【更新品牌与类别的关联】的数据访问，参数：{}", brandCategory);
        return brandCategoryMapper.updateById(brandCategory);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计品牌与类别的关联的数量】的数据访问，参数：无");
        return brandCategoryMapper.selectCount(null);
    }

    @Override
    public int countByBrand(Long brandId) {
        log.debug("开始执行【统计匹配品牌的品牌与类别的关联的数量】的数据访问，品牌：{}", brandId);
        QueryWrapper<BrandCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("brand_id", brandId);
        return brandCategoryMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByCategory(Long categoryId) {
        log.debug("开始执行【统计匹配类别的品牌与类别的关联的数量】的数据访问，类别：{}", categoryId);
        QueryWrapper<BrandCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id", categoryId);
        return brandCategoryMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByBrandAndCategory(Long brandId, Long categoryId) {
        log.debug("开始执行【统计匹配品牌与类别的品牌与类别的关联的数量】的数据访问，品牌：{}，类别：{}", brandId, categoryId);
        QueryWrapper<BrandCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("brand_id", brandId).eq("category_id", categoryId);
        return brandCategoryMapper.selectCount(queryWrapper);
    }

    @Override
    public BrandCategoryStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询品牌与类别的关联信息】的数据访问，参数：{}", id);
        return brandCategoryMapper.getStandardById(id);
    }

    @Override
    public PageData<BrandCategoryListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询品牌与类别的关联列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<BrandCategoryListItemVO> list = brandCategoryMapper.list();
        PageInfo<BrandCategoryListItemVO> pageInfo = new PageInfo<>(list);
        PageData<BrandCategoryListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
