package cn.tedu.csmall.product.dao.persist.mapper;

import cn.tedu.csmall.product.pojo.entity.Sku;
import cn.tedu.csmall.product.pojo.vo.SkuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SkuStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理SKU数据的Mapper接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Repository
public interface SkuMapper extends BaseMapper<Sku> {

    /**
     * 批量插入SKU数据
     *
     * @param skuList 若干个SKU数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Sku> skuList);

    /**
     * 根据id查询SKU数据详情
     *
     * @param id SKU ID
     * @return 匹配的SKU数据详情，如果没有匹配的数据，则返回null
     */
    SkuStandardVO getStandardById(Long id);

    /**
     * 根据SPU查询SKU列表
     *
     * @param spuId SPU ID
     * @return SKU列表
     */
    List<SkuListItemVO> listBySpuId(Long spuId);

}
