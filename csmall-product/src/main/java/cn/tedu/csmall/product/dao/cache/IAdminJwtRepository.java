package cn.tedu.csmall.product.dao.cache;

import cn.tedu.csmall.commons.pojo.po.AdminLoginInfoPO;

/**
 * 处理管理员数据缓存的访问接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IAdminJwtRepository {

    /**
     * 根据管理员的JWT获取登录信息
     *
     * @param jwt 当前管理员的JWT
     * @return 管理员的登录信息，如果没有匹配的数据，将返回null
     */
    AdminLoginInfoPO getLoginInfo(String jwt);

}
