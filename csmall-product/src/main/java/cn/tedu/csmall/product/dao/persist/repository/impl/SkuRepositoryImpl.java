package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.SkuMapper;
import cn.tedu.csmall.product.dao.persist.repository.ISkuRepository;
import cn.tedu.csmall.product.pojo.entity.Sku;
import cn.tedu.csmall.product.pojo.vo.SkuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SkuStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理SKU数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class SkuRepositoryImpl implements ISkuRepository {

    @Autowired
    private SkuMapper skuMapper;

    public SkuRepositoryImpl() {
        log.debug("创建数据访问实现类对象：SkuRepositoryImpl");
    }

    @Override
    public int insert(Sku sku) {
        log.debug("开始执行【插入SKU】的数据访问，参数：{}", sku);
        return skuMapper.insert(sku);
    }

    @Override
    public int insertBatch(List<Sku> skuList) {
        log.debug("开始执行【批量插入SKU】的数据访问，参数：{}", skuList);
        return skuMapper.insertBatch(skuList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除SKU】的数据访问，参数：{}", id);
        return skuMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除SKU】的数据访问，参数：{}", idList);
        return skuMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Sku sku) {
        log.debug("开始执行【更新SKU】的数据访问，参数：{}", sku);
        return skuMapper.updateById(sku);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计SKU的数量】的数据访问，参数：无");
        return skuMapper.selectCount(null);
    }

    @Override
    public int countByAlbum(Long albumId) {
        log.debug("开始执行【统计匹配相册的SKU的数量】的数据访问，相册：{}", albumId);
        QueryWrapper<Sku> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("album_id", albumId);
        return skuMapper.selectCount(queryWrapper);
    }

    @Override
    public SkuStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询SKU信息】的数据访问，参数：{}", id);
        return skuMapper.getStandardById(id);
    }

    @Override
    public PageData<SkuListItemVO> listBySpuId(Long spuId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行【根据SPU查询SKU列表】的数据访问，SPU：{}, 页码：{}，每页记录数：{}", spuId, pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<SkuListItemVO> list = skuMapper.listBySpuId(spuId);
        PageInfo<SkuListItemVO> pageInfo = new PageInfo<>(list);
        PageData<SkuListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
