package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.AttributeMapper;
import cn.tedu.csmall.product.dao.persist.repository.IAttributeRepository;
import cn.tedu.csmall.product.pojo.entity.Attribute;
import cn.tedu.csmall.product.pojo.vo.AttributeListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理属性数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class AttributeRepositoryImpl implements IAttributeRepository {

    @Autowired
    private AttributeMapper attributeMapper;

    public AttributeRepositoryImpl() {
        log.debug("创建数据访问实现类对象：AttributeRepositoryImpl");
    }

    @Override
    public int insert(Attribute attribute) {
        log.debug("开始执行【插入属性】的数据访问，参数：{}", attribute);
        return attributeMapper.insert(attribute);
    }

    @Override
    public int insertBatch(List<Attribute> attributeList) {
        log.debug("开始执行【批量插入属性】的数据访问，参数：{}", attributeList);
        return attributeMapper.insertBatch(attributeList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除属性】的数据访问，参数：{}", id);
        return attributeMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除属性】的数据访问，参数：{}", idList);
        return attributeMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Attribute attribute) {
        log.debug("开始执行【更新属性】的数据访问，参数：{}", attribute);
        return attributeMapper.updateById(attribute);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计属性的数量】的数据访问，参数：无");
        return attributeMapper.selectCount(null);
    }

    @Override
    public int countByTemplate(Long templateId) {
        log.debug("开始执行【统计匹配属性模板的属性的数量】的数据访问，属性模板：{}", templateId);
        QueryWrapper<Attribute> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("template_id", templateId);
        return attributeMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByNameAndTemplate(String name, Long templateId) {
        log.debug("开始执行【统计匹配名称且匹配属性模板的属性的数量】的数据访问，名称：{}，属性模板：{}", name, templateId);
        QueryWrapper<Attribute> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name).eq("template_id", templateId);
        return attributeMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByNameAndTemplateAndNotId(Long id, String name, Long templateId) {
        log.debug("开始执行【统计匹配名称且匹配属性模板但不匹配ID的属性的数量】的数据访问，ID：{}，名称：{}，属性模板：{}", id, name, templateId);
        QueryWrapper<Attribute> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name).eq("template_id", templateId).ne("id", id);
        return attributeMapper.selectCount(queryWrapper);
    }

    @Override
    public AttributeStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询属性信息】的数据访问，参数：{}", id);
        return attributeMapper.getStandardById(id);
    }

    @Override
    public PageData<AttributeListItemVO> listByTemplateId(Long templateId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行【根据属性模板查询属性列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<AttributeListItemVO> list = attributeMapper.listByTemplateId(templateId);
        PageInfo<AttributeListItemVO> pageInfo = new PageInfo<>(list);
        PageData<AttributeListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
