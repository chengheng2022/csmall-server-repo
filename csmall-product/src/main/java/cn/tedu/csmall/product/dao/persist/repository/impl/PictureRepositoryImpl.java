package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.PictureMapper;
import cn.tedu.csmall.product.dao.persist.repository.IPictureRepository;
import cn.tedu.csmall.product.pojo.entity.Picture;
import cn.tedu.csmall.product.pojo.vo.PictureListItemVO;
import cn.tedu.csmall.product.pojo.vo.PictureStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理图片数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class PictureRepositoryImpl implements IPictureRepository {

    @Autowired
    private PictureMapper pictureMapper;

    public PictureRepositoryImpl() {
        log.debug("创建数据访问实现类对象：PictureRepositoryImpl");
    }

    @Override
    public int insert(Picture picture) {
        log.debug("开始执行【插入图片】的数据访问，参数：{}", picture);
        return pictureMapper.insert(picture);
    }

    @Override
    public int insertBatch(List<Picture> pictureList) {
        log.debug("开始执行【批量插入图片】的数据访问，参数：{}", pictureList);
        return pictureMapper.insertBatch(pictureList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除图片】的数据访问，参数：{}", id);
        return pictureMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除图片】的数据访问，参数：{}", idList);
        return pictureMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Picture picture) {
        log.debug("开始执行【更新图片】的数据访问，参数：{}", picture);
        return pictureMapper.updateById(picture);
    }

    @Override
    public int updateNotCoverByAlbum(Long albumId) {
        log.debug("开始处理【根据相册ID将图片设置为\"非封面\"】的数据访问，参数：{}", albumId);
        Picture picture = new Picture();
        picture.setIsCover(0);
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("album_id", albumId);
        int rows = pictureMapper.update(picture, queryWrapper);
        return rows;
    }

    @Override
    public int count() {
        log.debug("开始执行【统计图片的数量】的数据访问，参数：无");
        return pictureMapper.selectCount(null);
    }

    @Override
    public int countByAlbum(Long albumId) {
        log.debug("开始执行【统计匹配相册的图片的数量】的数据访问，相册：{}", albumId);
        QueryWrapper<Picture> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("album_id", albumId);
        return pictureMapper.selectCount(queryWrapper);
    }

    @Override
    public PictureStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询图片信息】的数据访问，参数：{}", id);
        return pictureMapper.getStandardById(id);
    }

    @Override
    public PageData<PictureListItemVO> listByAlbumId(Long albumId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询图片列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<PictureListItemVO> list = pictureMapper.listByAlbumId(albumId);
        PageInfo<PictureListItemVO> pageInfo = new PageInfo<>(list);
        PageData<PictureListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
