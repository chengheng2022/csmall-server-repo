package cn.tedu.csmall.product.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * ID工具类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Component
public class IdUtils {

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    /**
     * 生成SPU的ID
     *
     * @return 自动编号的新ID
     */
    public synchronized long generateSpuId() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        return opsForValue.increment("spu_id");
    }

    /**
     * 生成SKU的ID
     *
     * @return 自动编号的新ID
     */
    public synchronized long generateSkuId() {
        ValueOperations<String, Serializable> opsForValue = redisTemplate.opsForValue();
        return opsForValue.increment("sku_id");
    }

}
