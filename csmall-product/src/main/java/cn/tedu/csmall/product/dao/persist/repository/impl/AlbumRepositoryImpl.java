package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.AlbumMapper;
import cn.tedu.csmall.product.dao.persist.repository.IAlbumRepository;
import cn.tedu.csmall.product.pojo.entity.Album;
import cn.tedu.csmall.product.pojo.vo.AlbumListItemVO;
import cn.tedu.csmall.product.pojo.vo.AlbumStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理相册数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class AlbumRepositoryImpl implements IAlbumRepository {

    @Autowired
    private AlbumMapper albumMapper;

    public AlbumRepositoryImpl() {
        log.debug("创建数据访问实现类对象：AlbumRepositoryImpl");
    }

    @Override
    public int insert(Album album) {
        log.debug("开始执行【插入相册】的数据访问，参数：{}", album);
        return albumMapper.insert(album);
    }

    @Override
    public int insertBatch(List<Album> albumList) {
        log.debug("开始执行【批量插入相册】的数据访问，参数：{}", albumList);
        return albumMapper.insertBatch(albumList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除相册】的数据访问，参数：{}", id);
        return albumMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除相册】的数据访问，参数：{}", idList);
        return albumMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Album album) {
        log.debug("开始执行【更新相册】的数据访问，参数：{}", album);
        return albumMapper.updateById(album);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计相册的数量】的数据访问，参数：无");
        return albumMapper.selectCount(null);
    }

    @Override
    public int countByName(String name) {
        log.debug("开始执行【统计匹配名称的相册的数量】的数据访问，参数：{}", name);
        QueryWrapper<Album> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return albumMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByNameAndNotId(Long id, String name) {
        log.debug("开始执行【统计匹配ID但不匹配名称的相册的数量】的数据访问，ID：{}，名称：{}", id, name);
        QueryWrapper<Album> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name).ne("id", id);
        return albumMapper.selectCount(queryWrapper);
    }

    @Override
    public AlbumStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询相册信息】的数据访问，参数：{}", id);
        return albumMapper.getStandardById(id);
    }

    @Override
    public PageData<AlbumListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询相册列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<AlbumListItemVO> list = albumMapper.list();
        PageInfo<AlbumListItemVO> pageInfo = new PageInfo<>(list);
        PageData<AlbumListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

}
