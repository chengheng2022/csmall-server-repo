package cn.tedu.csmall.product.service;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.param.AttributeAddNewParam;
import cn.tedu.csmall.product.pojo.param.AttributeUpdateInfoParam;
import cn.tedu.csmall.product.pojo.vo.AttributeListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeStandardVO;
import org.springframework.transaction.annotation.Transactional;

/**
 * 属性业务接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Transactional
public interface IAttributeService {

    /**
     * 添加属性
     *
     * @param attributeAddNewParam 添加的属性对象
     */
    void addNew(AttributeAddNewParam attributeAddNewParam);

    /**
     * 根据ID删除属性
     *
     * @param id 属性ID
     */
    void delete(Long id);

    /**
     * 修改属性基本资料
     *
     * @param id                       属性ID
     * @param attributeUpdateInfoParam 封装了新基本资料的对象
     */
    void updateInfoById(Long id, AttributeUpdateInfoParam attributeUpdateInfoParam);

    /**
     * 根据id获取属性的标准信息
     *
     * @param id 属性id
     * @return 匹配的属性的标准信息，如果没有匹配的数据，将返回null
     */
    AttributeStandardVO getStandardById(Long id);

    /**
     * 根据属性模板id查询属性列表
     *
     * @param templateId 属性模板ID
     * @return 属性列表的集合
     */
    PageData<AttributeListItemVO> listByTemplateId(Long templateId);

}
