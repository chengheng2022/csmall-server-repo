package cn.tedu.csmall.product.service.impl;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.web.ServiceCode;
import cn.tedu.csmall.product.dao.persist.repository.IBrandCategoryRepository;
import cn.tedu.csmall.product.dao.persist.repository.IBrandRepository;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.param.BrandAddNewParam;
import cn.tedu.csmall.product.pojo.param.BrandUpdateInfoParam;
import cn.tedu.csmall.product.pojo.vo.BrandListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import cn.tedu.csmall.product.service.IBrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 处理品牌业务的实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Service
public class BrandServiceImpl implements IBrandService {

    @Value("${csmall.crud.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private IBrandRepository brandRepository;
    @Autowired
    private ISpuRepository spuRepository;
    @Autowired
    private IBrandCategoryRepository brandCategoryRepository;

    public BrandServiceImpl() {
        log.info("创建业务对象：BrandServiceImpl");
    }

    @Override
    public void addNew(BrandAddNewParam brandAddNewParam) {
        log.debug("开始处理【添加品牌】的业务，参数：{}", brandAddNewParam);
        // 应该保证此品牌的名称是唯一的
        String name = brandAddNewParam.getName();
        int count = brandRepository.countByName(name);
        if (count > 0) {
            String message = "添加品牌失败，品牌名称【" + brandAddNewParam.getName() + "】已经被占用！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 创建品牌对象，用于插入到数据表
        Brand brand = new Brand();
        // 将参数DTO的各属性值复制到实体类对象中
        BeanUtils.copyProperties(brandAddNewParam, brand);
        // 插入数据
        log.debug("即将向数据库中插入数据：{}", brand);
        int rows = brandRepository.insert(brand);
        if (rows != 1) {
            String message = "添加品牌失败，服务器忙，请稍后再尝试！";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void delete(Long id) {
        log.debug("开始处理【根据ID删除品牌】的业务，参数：{}", id);
        // 检查尝试删除的数据是否存在
        Object queryResult = brandRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "删除品牌失败，尝试删除的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 检查此品牌是否关联了品牌
        {
            int count = brandCategoryRepository.countByBrand(id);
            if (count > 0) {
                String message = "删除品牌失败！当前品牌仍关联了品牌！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 检查此品牌是否关联了SPU
        {
            int count = spuRepository.countByBrand(id);
            if (count > 0) {
                String message = "删除品牌失败！当前品牌仍关联了商品！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 执行删除
        log.debug("即将执行删除数据，参数：{}", id);
        int rows = brandRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除品牌失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }
    }

    @Override
    public void updateInfoById(Long id, BrandUpdateInfoParam brandUpdateInfoParam) {
        log.debug("开始处理【修改品牌详情】的业务，参数ID：{}, 新数据：{}", id, brandUpdateInfoParam);
        // 检查名称是否被占用
        {
            int count = brandRepository.countByNameAndNotId(id, brandUpdateInfoParam.getName());
            if (count > 0) {
                String message = "修改品牌详情失败，品牌名称已经被占用！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 调用Mapper对象的getDetailsById()方法执行查询
        BrandStandardVO queryResult = brandRepository.getStandardById(id);
        // 判断查询结果是否为null
        if (queryResult == null) {
            // 是：此id对应的数据不存在，则抛出异常(ERROR_NOT_FOUND)
            String message = "修改品牌详情失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        Brand brand = new Brand();
        BeanUtils.copyProperties(brandUpdateInfoParam, brand);
        brand.setId(id);

        // 修改数据
        log.debug("即将修改数据：{}", brand);
        int rows = brandRepository.update(brand);
        if (rows != 1) {
            String message = "修改品牌详情失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

    @Override
    public void setEnable(Long id) {
        updateEnableById(id, 1);
    }

    @Override
    public void setDisable(Long id) {
        updateEnableById(id, 0);
    }

    @Override
    public BrandStandardVO getStandardById(Long id) {
        log.debug("开始处理【根据ID查询品牌详情】的业务，参数：{}", id);
        BrandStandardVO queryResult = brandRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "根据id查询品牌详情失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        log.debug("即将返回品牌详情：{}", queryResult);
        return queryResult;
    }

    @Override
    public PageData<BrandListItemVO> list(Integer pageNum) {
        log.debug("开始处理【查询品牌列表】的业务，页码：{}", pageNum);
        PageData<BrandListItemVO> pageData = brandRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<BrandListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理【查询品牌列表】的业务，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageData<BrandListItemVO> pageData = brandRepository.list(pageNum, pageSize);
        return pageData;
    }

    private void updateEnableById(Long id, Integer enable) {
        log.debug("开始处理【{}品牌】的业务，ID：{}，目标状态：{}", ENABLE_TEXT[enable], id, enable);
        // 检查数据是否存在
        BrandStandardVO queryResult = brandRepository.getStandardById(id);
        if (queryResult == null) {
            String message = ENABLE_TEXT[enable] + "品牌失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 检查当前状态是否与参数表示的状态相同
        if (queryResult.getEnable().equals(enable)) {
            String message = ENABLE_TEXT[enable] + "品牌失败，当前品牌已经处于" + ENABLE_TEXT[enable] + "状态！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 准备执行更新
        Brand brand = new Brand();
        brand.setId(id);
        brand.setEnable(enable);
        log.debug("即将修改数据，参数：{}", brand);
        int rows = brandRepository.update(brand);
        if (rows != 1) {
            String message = ENABLE_TEXT[enable] + "品牌失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

}
