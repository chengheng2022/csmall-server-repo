package cn.tedu.csmall.product.service.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.dao.persist.repository.IAttributeRepository;
import cn.tedu.csmall.product.dao.persist.repository.IAttributeTemplateRepository;
import cn.tedu.csmall.product.dao.persist.repository.ICategoryAttributeTemplateRepository;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.param.AttributeTemplateAddNewParam;
import cn.tedu.csmall.product.pojo.param.AttributeTemplateUpdateInfoParam;
import cn.tedu.csmall.product.pojo.entity.AttributeTemplate;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateStandardVO;
import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.product.service.IAttributeTemplateService;
import cn.tedu.csmall.commons.web.ServiceCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 处理属性模板业务的实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Service
public class AttributeTemplateServiceImpl implements IAttributeTemplateService {

    @Value("${csmall.crud.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private IAttributeTemplateRepository attributeTemplateRepository;
    @Autowired
    private IAttributeRepository attributeRepository;
    @Autowired
    private ICategoryAttributeTemplateRepository categoryAttributeTemplateRepository;
    @Autowired
    private ISpuRepository spuRepository;

    public AttributeTemplateServiceImpl() {
        log.info("创建业务对象：AttributeTemplateServiceImpl");
    }

    @Override
    public void addNew(AttributeTemplateAddNewParam attributeTemplateAddNewParam) {
        log.debug("开始处理【添加属性模板】的业务，参数：{}", attributeTemplateAddNewParam);
        // 应该保证此属性模板的名称是唯一的
        String name = attributeTemplateAddNewParam.getName();
        int count = attributeTemplateRepository.countByName(name);
        if (count > 0) {
            String message = "添加属性模板失败，属性模板名称【" + name + "】已经被占用！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 创建对象
        AttributeTemplate attributeTemplate = new AttributeTemplate();
        // 将参数DTO的各属性值复制到实体类对象中
        BeanUtils.copyProperties(attributeTemplateAddNewParam, attributeTemplate);
        // 插入数据
        log.debug("准备向数据库中写入属性模板数据：{}", attributeTemplate);
        int rows = attributeTemplateRepository.insert(attributeTemplate);
        if (rows != 1) {
            String message = "添加属性模板失败，服务器忙，请稍后再尝试！";
            log.debug(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void delete(Long id) {
        log.debug("开始处理【根据ID删除属性模板】的业务，参数：{}", id);
        // 检查尝试删除的属性是否存在
        Object queryResult = attributeTemplateRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "删除属性模板失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 如果有属性关联到了此属性模板，不允许删除
        {
            int count = attributeRepository.countByTemplate(id);
            if (count > 0) {
                String message = "删除属性模板失败！当前属性模板仍存在关联的属性！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 如果有属性模板关联到了此属性模板，不允许删除
        {
            int count = categoryAttributeTemplateRepository.countByAttributeTemplate(id);
            if (count > 0) {
                String message = "删除属性模板失败！当前属性模板仍存在关联的属性模板！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 如果有SPU关联到了此属性模板，不允许删除
        {
            int count = spuRepository.countByAttributeTemplate(id);
            if (count > 0) {
                String message = "删除属性模板失败！当前属性模板仍存在关联的SPU！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 执行删除
        log.debug("即将执行删除数据，参数：{}", id);
        int rows = attributeTemplateRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除属性模板失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void updateInfoById(Long id, AttributeTemplateUpdateInfoParam attributeTemplateUpdateInfoParam) {
        log.debug("开始处理【修改属性模板详情】的业务，参数ID：{}, 新数据：{}", id, attributeTemplateUpdateInfoParam);
        // 检查名称是否被占用
        {
            int count = attributeTemplateRepository.countByNameAndNotId(id, attributeTemplateUpdateInfoParam.getName());
            if (count > 0) {
                String message = "修改属性模板详情失败，属性模板名称已经被占用！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
            }
        }

        // 调用adminMapper根据参数id执行查询
        AttributeTemplateStandardVO queryResult = attributeTemplateRepository.getStandardById(id);
        // 判断查询结果是否为null
        if (queryResult == null) {
            // 抛出ServiceException，业务状态码：40400
            String message = "修改属性模板详情失败！尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 创建Admin对象，将作为修改时的参数
        AttributeTemplate attributeTemplate = new AttributeTemplate();
        BeanUtils.copyProperties(attributeTemplateUpdateInfoParam, attributeTemplate);
        attributeTemplate.setId(id);

        // 调用Mapper对象的update()修改属性模板基本资料，并获取返回值
        log.debug("即将修改属性模板详情：{}", attributeTemplate);
        int rows = attributeTemplateRepository.update(attributeTemplate);
        // 判断返回值是否不等于1
        if (rows != 1) {
            // 是：抛出ServiceException（ERROR_INSERT）
            String message = "修改属性模板详情失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

    @Override
    public AttributeTemplateStandardVO getStandardById(Long id) {
        log.debug("开始处理【根据ID查询属性模板详情】的业务");
        AttributeTemplateStandardVO attributeTemplate = attributeTemplateRepository.getStandardById(id);
        if (attributeTemplate == null) {
            // 是：此id对应的数据不存在，则抛出异常(ERROR_NOT_FOUND)
            String message = "查询属性模板详情失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return attributeTemplate;
    }

    @Override
    public PageData<AttributeTemplateListItemVO> list(Integer pageNum) {
        log.debug("开始处理【查询属性模板列表】的业务，页码：{}", pageNum);
        PageData<AttributeTemplateListItemVO> pageData = attributeTemplateRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<AttributeTemplateListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理【查询属性模板列表】的业务，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageData<AttributeTemplateListItemVO> pageData = attributeTemplateRepository.list(pageNum, pageSize);
        return pageData;
    }

}
