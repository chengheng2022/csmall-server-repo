package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.SpuMapper;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.entity.Spu;
import cn.tedu.csmall.product.pojo.vo.SpuFullInfoVO;
import cn.tedu.csmall.product.pojo.vo.SpuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SpuStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理SPU数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class SpuRepositoryImpl implements ISpuRepository {

    @Autowired
    private SpuMapper spuMapper;

    public SpuRepositoryImpl() {
        log.debug("创建数据访问实现类对象：SpuRepositoryImpl");
    }

    @Override
    public int insert(Spu spu) {
        log.debug("开始执行【插入SPU】的数据访问，参数：{}", spu);
        return spuMapper.insert(spu);
    }

    @Override
    public int insertBatch(List<Spu> spuList) {
        log.debug("开始执行【批量插入SPU】的数据访问，参数：{}", spuList);
        return spuMapper.insertBatch(spuList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除SPU】的数据访问，参数：{}", id);
        return spuMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除SPU】的数据访问，参数：{}", idList);
        return spuMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Spu spu) {
        log.debug("开始执行【更新SPU】的数据访问，参数：{}", spu);
        return spuMapper.updateById(spu);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计SPU的数量】的数据访问，参数：无");
        return spuMapper.selectCount(null);
    }

    @Override
    public int countByAlbum(Long albumId) {
        log.debug("开始执行【统计匹配相册的SPU的数量】的数据访问，相册：{}", albumId);
        QueryWrapper<Spu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("album_id", albumId);
        return spuMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByBrand(Long brandId) {
        log.debug("开始执行【统计匹配品牌的SPU的数量】的数据访问，品牌：{}", brandId);
        QueryWrapper<Spu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("brand_id", brandId);
        return spuMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByCategory(Long categoryId) {
        log.debug("开始执行【统计匹配类别的SPU的数量】的数据访问，类别：{}", categoryId);
        QueryWrapper<Spu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id", categoryId);
        return spuMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByAttributeTemplate(Long attributeTemplateId) {
        log.debug("开始执行【统计匹配属性模板的SPU的数量】的数据访问，属性模板：{}", attributeTemplateId);
        QueryWrapper<Spu> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("attribute_template_id", attributeTemplateId);
        return spuMapper.selectCount(queryWrapper);
    }

    @Override
    public SpuStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询SPU信息】的数据访问，参数：{}", id);
        return spuMapper.getStandardById(id);
    }

    @Override
    public SpuFullInfoVO getFullInfoById(Long id) {
        log.debug("开始执行【根据ID查询SPU完整信息】的数据访问，参数：{}", id);
        return spuMapper.getFullInfoById(id);
    }

    @Override
    public PageData<SpuListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询SPU列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<SpuListItemVO> list = spuMapper.list();
        PageInfo<SpuListItemVO> pageInfo = new PageInfo<>(list);
        PageData<SpuListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
