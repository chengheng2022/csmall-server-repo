package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.vo.BrandListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 处理品牌数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IBrandRepository {

    /**
     * 插入品牌数据
     *
     * @param brand 品牌数据
     * @return 受影响的行数
     */
    int insert(Brand brand);

    /**
     * 批量插入品牌数据
     *
     * @param brandList 若干个品牌数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Brand> brandList);

    /**
     * 根据id删除品牌数据
     *
     * @param id 品牌ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除品牌
     *
     * @param idList 需要删除的若干个品牌的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新品牌数据
     *
     * @param brand 封装了品牌的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(Brand brand);

    /**
     * 统计品牌数据的数量
     *
     * @return 品牌数据的数量
     */
    int count();

    /**
     * 根据品牌名称统计数据的数量
     *
     * @param name 品牌名称
     * @return 匹配名称的品牌数据的数量
     */
    int countByName(String name);

    /**
     * 统计当前表中非此品牌id的匹配名称的品牌数据的数量
     *
     * @param id   当前品牌ID
     * @param name 品牌名称
     * @return 当前表中非此品牌id的匹配名称的品牌数据的数量
     */
    int countByNameAndNotId(@Param("id") Long id, @Param("name") String name);

    /**
     * 根据id查询品牌数据详情
     *
     * @param id 品牌ID
     * @return 匹配的品牌数据详情，如果没有匹配的数据，则返回null
     */
    BrandStandardVO getStandardById(Long id);

    /**
     * 查询品牌数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 品牌数据列表
     */
    PageData<BrandListItemVO> list(Integer pageNum, Integer pageSize);

}
