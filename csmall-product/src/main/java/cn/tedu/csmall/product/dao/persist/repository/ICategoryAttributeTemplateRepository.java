package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.CategoryAttributeTemplate;
import cn.tedu.csmall.product.pojo.vo.CategoryAttributeTemplateListItemVO;
import cn.tedu.csmall.product.pojo.vo.CategoryAttributeTemplateStandardVO;

import java.util.Collection;
import java.util.List;

/**
 * 处理类别与属性模板的数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface ICategoryAttributeTemplateRepository {


    /**
     * 插入类别与属性模板的关联数据
     *
     * @param categoryAttributeTemplate 类别与属性模板的关联数据
     * @return 受影响的行数
     */
    int insert(CategoryAttributeTemplate categoryAttributeTemplate);

    /**
     * 批量插入类别与属性模板的关联数据
     *
     * @param categoryAttributeTemplateList 若干个类别与属性模板的关联数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<CategoryAttributeTemplate> categoryAttributeTemplateList);

    /**
     * 根据id删除类别与属性模板的关联数据
     *
     * @param id 类别与属性模板的关联数据的ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除类别与属性模板的关联数据
     *
     * @param idList 需要删除的若干个类别与属性模板的关联数据的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新类别与属性模板的关联数据
     *
     * @param categoryAttributeTemplate 封装了类别与属性模板的关联数据的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(CategoryAttributeTemplate categoryAttributeTemplate);

    /**
     * 统计类别与属性模板的关联数据的数量
     *
     * @return 类别与属性模板的关联数据的数量
     */
    int count();

    /**
     * 根据类别统计关联数据的数量
     *
     * @param categoryId 类别ID
     * @return 此类别关联的数据的数量
     */
    int countByCategory(Long categoryId);

    /**
     * 根据属性模板统计关联数据的数量
     *
     * @param attributeTemplateId 属性模板ID
     * @return 此属性模板关联的数据的数量
     */
    int countByAttributeTemplate(Long attributeTemplateId);

    /**
     * 根据id查询类别与属性模板的关联数据详情
     *
     * @param id 类别与属性模板的关联ID
     * @return 匹配的类别与属性模板的关联数据详情，如果没有匹配的数据，则返回null
     */
    CategoryAttributeTemplateStandardVO getStandardById(Long id);

    /**
     * 查询类别与属性模板的关联数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 类别与属性模板的关联数据列表
     */
    PageData<CategoryAttributeTemplateListItemVO> list(Integer pageNum, Integer pageSize);

}
