package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.product.dao.persist.mapper.SpuDetailMapper;
import cn.tedu.csmall.product.dao.persist.repository.ISpuDetailRepository;
import cn.tedu.csmall.product.pojo.entity.SpuDetail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * 处理SPU详情数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class SpuDetailRepositoryImpl implements ISpuDetailRepository {

    @Autowired
    private SpuDetailMapper spuDetailMapper;

    public SpuDetailRepositoryImpl() {
        log.debug("创建数据访问实现类对象：SpuDetailRepositoryImpl");
    }

    @Override
    public int insert(SpuDetail spuDetail) {
        log.debug("开始执行【插入SPU详情】的数据访问，参数：{}", spuDetail);
        return spuDetailMapper.insert(spuDetail);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除SPU详情】的数据访问，参数：{}", id);
        return spuDetailMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除SPU详情】的数据访问，参数：{}", idList);
        return spuDetailMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(SpuDetail spuDetail) {
        log.debug("开始执行【更新SPU详情】的数据访问，参数：{}", spuDetail);
        return spuDetailMapper.updateById(spuDetail);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计SPU详情的数量】的数据访问，参数：无");
        return spuDetailMapper.selectCount(null);
    }

}
