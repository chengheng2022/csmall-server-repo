package cn.tedu.csmall.product.preload;

import cn.tedu.csmall.product.service.IAlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * 缓存预热类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 * @deprecated 0.0.1
 */
@Slf4j
@Deprecated // 声明为已过期
// @Component
public class CachePreload implements ApplicationRunner {

    // =========================================================
    // 【已过期】不再使用
    // 【注意】此类只是用于演示Spring Boot中启动时自动执行任务的DEMO类
    // =========================================================

    @Autowired
    private IAlbumService albumService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.debug("开始执行CachePreload.run()");
        albumService.rebuildCache();
    }

}
