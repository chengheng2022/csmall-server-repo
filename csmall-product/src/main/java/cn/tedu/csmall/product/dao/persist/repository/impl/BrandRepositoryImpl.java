package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.BrandMapper;
import cn.tedu.csmall.product.dao.persist.repository.IBrandRepository;
import cn.tedu.csmall.product.pojo.entity.Brand;
import cn.tedu.csmall.product.pojo.vo.BrandListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理品牌数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class BrandRepositoryImpl implements IBrandRepository {

    @Autowired
    private BrandMapper brandMapper;

    public BrandRepositoryImpl() {
        log.debug("创建数据访问实现类对象：BrandRepositoryImpl");
    }

    @Override
    public int insert(Brand brand) {
        log.debug("开始执行【插入品牌】的数据访问，参数：{}", brand);
        return brandMapper.insert(brand);
    }

    @Override
    public int insertBatch(List<Brand> brandList) {
        log.debug("开始执行【批量插入品牌】的数据访问，参数：{}", brandList);
        return brandMapper.insertBatch(brandList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除品牌】的数据访问，参数：{}", id);
        return brandMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除品牌】的数据访问，参数：{}", idList);
        return brandMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(Brand brand) {
        log.debug("开始执行【更新品牌】的数据访问，参数：{}", brand);
        return brandMapper.updateById(brand);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计品牌的数量】的数据访问，参数：无");
        return brandMapper.selectCount(null);
    }

    @Override
    public int countByName(String name) {
        log.debug("开始执行【统计匹配名称的品牌的数量】的数据访问，名称：{}", name);
        QueryWrapper<Brand> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return brandMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByNameAndNotId(Long id, String name) {
        log.debug("开始执行【统计匹配名称但不匹配ID的品牌的数量】的数据访问，ID：{}，名称：{}", id, name);
        QueryWrapper<Brand> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name).ne("id", id);
        return brandMapper.selectCount(queryWrapper);
    }

    @Override
    public BrandStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询品牌信息】的数据访问，参数：{}", id);
        return brandMapper.getStandardById(id);
    }

    @Override
    public PageData<BrandListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询品牌列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<BrandListItemVO> list = brandMapper.list();
        PageInfo<BrandListItemVO> pageInfo = new PageInfo<>(list);
        PageData<BrandListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
