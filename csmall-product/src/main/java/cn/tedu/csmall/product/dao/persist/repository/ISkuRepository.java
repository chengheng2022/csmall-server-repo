package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.Sku;
import cn.tedu.csmall.product.pojo.vo.SkuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SkuStandardVO;

import java.util.Collection;
import java.util.List;

/**
 * 处理SKU数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface ISkuRepository {

    /**
     * 插入SKU数据
     *
     * @param sku SKU数据
     * @return 受影响的行数
     */
    int insert(Sku sku);

    /**
     * 批量插入SKU数据
     *
     * @param skuList 若干个SKU数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Sku> skuList);

    /**
     * 根据id删除SKU数据
     *
     * @param id SKU ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除SKU数据
     *
     * @param idList 需要删除的若干个SKU的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新SKU数据
     *
     * @param sku 封装了SKU的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(Sku sku);

    /**
     * 统计SKU数据的数量
     *
     * @return SKU数据的数量
     */
    int count();

    /**
     * 根据相册统计SKU数据的数量
     *
     * @param albumId 相册ID
     * @return 与此相册关联的图片数据的数量
     */
    int countByAlbum(Long albumId);

    /**
     * 根据ID查询SKU数据详情
     *
     * @param id SKU ID
     * @return 匹配的SKU数据详情，如果没有匹配的数据，则返回null
     */
    SkuStandardVO getStandardById(Long id);

    /**
     * 根据SPU查询SKU列表
     *
     * @param spuId SPU ID
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return SKU列表
     */
    PageData<SkuListItemVO> listBySpuId(Long spuId, Integer pageNum, Integer pageSize);

}
