package cn.tedu.csmall.product.schedule;

import cn.tedu.csmall.product.service.IAlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * 处理缓存的计划任务类
 */
@Slf4j
// @Component
public class CacheSchedule {

    @Autowired
    private IAlbumService albumService;

    public CacheSchedule() {
        log.debug("创建计划任务类对象：CacheSchedule");
    }

    // fixedRate：执行频率，以上一次执行的起始时间开始计算下一次的执行时间，以毫秒为单位
    // fixedDelay：执行间隔，以上一次执行的结束时间开始计算下一次的执行时间，以毫秒为单位
    // cron：取值是一个字符串，此字符串是一个表达式，包含6~7个值，各值之间使用空格进行分隔
    // -- 在cron表达式中，各值表示的意义依次是：秒 分 时 日 月 星期 [年]
    // -- 各值均可使用通配符表示
    // -- 使用星号（*）表示任意值
    // -- 使用问号（?）表示不关心此值，此通配符仅能用于“日”和“星期”的值
    // -- 例如：56 34 12 13 2 ? 2023，表示2023年3月13日12:34:56会执行此任务，无视当天星期几
    // -- 在cron表达式中的各值还可以使用 x/y 格式的值，例如在“分”的位置配置 1/5，表示分钟值为1时执行，且每间隔5分钟执行1次
    @Scheduled(fixedRate = 3 * 60 * 1000)
    public void rebuildCache() {
        log.debug("执行了计划任务……");

        albumService.rebuildCache();
    }

    // 【CRON表达式示例】
    // “30 * * * * ?” 每到半分钟触发任务
    // “30 10 * * * ?” 每小时的10分30秒触发任务
    // “30 10 1 * * ?” 每天1点10分30秒触发任务
    // “30 10 1 20 * ?” 每月20号1点10分30秒触发任务
    // “30 10 1 20 10 ? *” 每年10月20号1点10分30秒触发任务
    // “30 10 1 20 10 ? 2011” 2011年10月20号1点10分30秒触发任务
    // “30 10 1 ? 10 * 2011” 2011年10月每天1点10分30秒触发任务
    // “30 10 1 ? 10 SUN 2011” 2011年10月每周日1点10分30秒触发任务
    // “15,30,45 * * * * ?” 每到15秒，或30秒，或45秒时触发任务
    // “15-45 * * * * ?” 15到45秒内，每秒都触发任务
    // “15/5 * * * * ?” 每分钟的每15秒开始触发，每隔5秒触发一次
    // “15-30/5 * * * * ?” 每分钟的15秒到30秒之间开始触发，每隔5秒触发一次
    // “0 0/3 * * * ?” 每小时的第0分0秒开始，每三分钟触发一次
    // “0 15 10 ? * MON-FRI” 星期一到星期五的10点15分0秒触发任务
    // “0 15 10 L * ?” 每个月最后一天的10点15分0秒触发任务
    // “0 15 10 LW * ?” 每个月最后一个工作日的10点15分0秒触发任务
    // “0 15 10 ? * 5L” 每个月最后一个星期四的10点15分0秒触发任务
    // “0 15 10 ? * 5#3” 每个月第三周的星期四的10点15分0秒触发任务
    // 参考：https://blog.csdn.net/stone_bk/article/details/124005751

}
