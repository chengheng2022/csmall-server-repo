package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.AttributeTemplate;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateStandardVO;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 处理属性模板数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IAttributeTemplateRepository {

    /**
     * 插入属性模板数据
     *
     * @param attributeTemplate 属性模板数据
     * @return 受影响的行数
     */
    int insert(AttributeTemplate attributeTemplate);

    /**
     * 批量插入属性模板数据
     *
     * @param attributeTemplateList 若干个属性模板数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<AttributeTemplate> attributeTemplateList);

    /**
     * 根据id删除属性模板数据
     *
     * @param id 属性模板ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除属性模板
     *
     * @param idList 需要删除的若干个属性模板的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 根据id修改属性模板数据详情
     *
     * @param attributeTemplate 封装了ID与新数据的属性模板对象
     * @return 受影响的行数
     */
    int update(AttributeTemplate attributeTemplate);

    /**
     * 统计属性模板数据的数量
     *
     * @return 属性模板数据的数量
     */
    int count();

    /**
     * 根据属性模板名称统计当前表中属性模板数据的数量
     *
     * @param name 属性模板名称
     * @return 当前表中匹配名称的属性模板数据的数量
     */
    int countByName(String name);

    /**
     * 统计当前表中非此属性模板id的匹配名称的属性模板数据的数量
     *
     * @param id   当前属性模板ID
     * @param name 属性模板名称
     * @return 当前表中非此属性模板id的匹配名称的属性模板数据的数量
     */
    int countByNameAndNotId(@Param("id") Long id, @Param("name") String name);

    /**
     * 根据id查询属性模板数据详情
     *
     * @param id 属性模板ID
     * @return 匹配的属性模板数据详情，如果没有匹配的数据，则返回null
     */
    AttributeTemplateStandardVO getStandardById(Long id);

    /**
     * 查询属性模板数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 属性模板数据列表
     */
    PageData<AttributeTemplateListItemVO> list(Integer pageNum, Integer pageSize);

}
