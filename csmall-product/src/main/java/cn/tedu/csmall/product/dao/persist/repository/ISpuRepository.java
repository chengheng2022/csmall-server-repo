package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.Spu;
import cn.tedu.csmall.product.pojo.vo.SpuFullInfoVO;
import cn.tedu.csmall.product.pojo.vo.SpuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SpuStandardVO;

import java.util.Collection;
import java.util.List;

/**
 * 处理SPU数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface ISpuRepository {

    /**
     * 插入SPU数据
     *
     * @param spu SPU数据
     * @return 受影响的行数
     */
    int insert(Spu spu);

    /**
     * 批量插入SPU数据
     *
     * @param spuList 若干个SPU数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Spu> spuList);

    /**
     * 根据ID删除SPU数据
     *
     * @param id SPU ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除SPU
     *
     * @param idList 需要删除的若干个SPU的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新SPU数据
     *
     * @param spu 封装了SPU的id和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(Spu spu);

    /**
     * 统计SPU数据的数量
     *
     * @return SPU数据的数量
     */
    int count();

    /**
     * 根据相册统计SPU数量
     *
     * @param albumId 相册ID
     * @return 关联了此相册的SPU数量
     */
    int countByAlbum(Long albumId);

    /**
     * 根据品牌统计SPU数量
     *
     * @param brandId 品牌ID
     * @return 关联了此品牌的SPU数量
     */
    int countByBrand(Long brandId);

    /**
     * 根据类别统计SPU数量
     *
     * @param categoryId 品牌ID
     * @return 关联了此类别的SPU数量
     */
    int countByCategory(Long categoryId);

    /**
     * 根据属性模板统计SPU数量
     *
     * @param attributeTemplateId 属性模板ID
     * @return 此属性模板关联的数据的SPU数量
     */
    int countByAttributeTemplate(Long attributeTemplateId);

    /**
     * 根据ID查询SPU标准信息
     *
     * @param id SPU ID
     * @return 匹配的SPU的标准信息，如果没有匹配的数据，则返回null
     */
    SpuStandardVO getStandardById(Long id);

    /**
     * 根据ID查询SPU完整信息
     *
     * @param id SPU ID
     * @return 匹配的SPU的完整信息，如果没有匹配的数据，则返回null
     */
    SpuFullInfoVO getFullInfoById(Long id);

    /**
     * 查询SPU数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return SPU数据列表
     */
    PageData<SpuListItemVO> list(Integer pageNum, Integer pageSize);

}
