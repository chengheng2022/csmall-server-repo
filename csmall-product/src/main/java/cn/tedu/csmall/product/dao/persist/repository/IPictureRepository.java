package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.Picture;
import cn.tedu.csmall.product.pojo.vo.PictureListItemVO;
import cn.tedu.csmall.product.pojo.vo.PictureStandardVO;

import java.util.Collection;
import java.util.List;

/**
 * 处理图片数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IPictureRepository {

    /**
     * 插入图片数据
     *
     * @param picture 图片数据
     * @return 受影响的行数
     */
    int insert(Picture picture);

    /**
     * 批量插入图片数据
     *
     * @param pictureList 若干个图片数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Picture> pictureList);

    /**
     * 根据id删除图片数据
     *
     * @param id 图片ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除图片数据
     *
     * @param idList 需要删除的若干个图片的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新图片数据
     *
     * @param picture 封装了图片的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(Picture picture);

    /**
     * 根据相册ID将图片设置为"非封面"
     *
     * @param albumId 相册ID
     * @return 受影响的行数
     */
    int updateNotCoverByAlbum(Long albumId);

    /**
     * 统计图片数据的数量
     *
     * @return 图片数据的数量
     */
    int count();

    /**
     * 根据相册统计图片数据的数量
     *
     * @param albumId 相册ID
     * @return 与此相册关联的图片数据的数量
     */
    int countByAlbum(Long albumId);

    /**
     * 根据id查询图片数据详情
     *
     * @param id 图片ID
     * @return 匹配的图片数据详情，如果没有匹配的数据，则返回null
     */
    PictureStandardVO getStandardById(Long id);

    /**
     * 根据相册ID查询图片数据列表
     *
     * @param albumId  相册ID
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 图片数据列表
     */
    PageData<PictureListItemVO> listByAlbumId(Long albumId, Integer pageNum, Integer pageSize);
}
