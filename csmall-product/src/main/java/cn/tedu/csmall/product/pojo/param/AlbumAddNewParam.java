package cn.tedu.csmall.product.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 添加相册的参数类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Data
@Accessors(chain = true)
public class AlbumAddNewParam implements Serializable {

    /**
     * 相册名称
     */
    @ApiModelProperty(value = "相册名称", required = true)
    @NotEmpty(message = "添加相册失败，必须提交相册名称！")
    private String name;

    /**
     * 相册简介
     */
    @ApiModelProperty(value = "相册简介", required = true)
    @NotEmpty(message = "添加相册失败，必须提交相册简介！")
    private String description;

    /**
     * 排序序号
     */
    @ApiModelProperty(value = "排序序号", required = true)
    @NotNull(message = "添加相册失败，必须提交排序序号！")
    @Min(value = 0, message = "添加相册失败，排序序号不得小于0！")
    @Max(value = 99, message = "添加相册失败，排序序号不得大于99！")
    private Integer sort;

}