package cn.tedu.csmall.product.dao.persist.mapper;

import cn.tedu.csmall.product.pojo.entity.Spu;
import cn.tedu.csmall.product.pojo.vo.SpuFullInfoVO;
import cn.tedu.csmall.product.pojo.vo.SpuListItemVO;
import cn.tedu.csmall.product.pojo.vo.SpuStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理SPU数据的Mapper接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Repository
public interface SpuMapper extends BaseMapper<Spu> {

    /**
     * 批量插入SPU数据
     *
     * @param spuList 若干个SPU数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Spu> spuList);

    /**
     * 根据ID查询SPU标准信息
     *
     * @param id SPU ID
     * @return 匹配的SPU的标准信息，如果没有匹配的数据，则返回null
     */
    SpuStandardVO getStandardById(Long id);

    /**
     * 根据ID查询SPU完整信息
     *
     * @param id SPU ID
     * @return 匹配的SPU的完整信息，如果没有匹配的数据，则返回null
     */
    SpuFullInfoVO getFullInfoById(Long id);

    /**
     * 查询SPU列表
     *
     * @return SPU列表
     */
    List<SpuListItemVO> list();

}
