package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.product.pojo.entity.SpuDetail;

import java.util.Collection;

/**
 * 处理SPU详情数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface ISpuDetailRepository {

    /**
     * 插入SPU详情数据
     *
     * @param spuDetail SPU详情数据
     * @return 受影响的行数
     */
    int insert(SpuDetail spuDetail);

    /**
     * 根据id删除SPU详情数据
     *
     * @param id SPU详情ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除SPU详情
     *
     * @param idList 需要删除的若干个SPU详情的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新SPU详情数据
     *
     * @param spuDetail 封装了SPU详情的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(SpuDetail spuDetail);

    /**
     * 统计SPU详情数据的数量
     *
     * @return SPU详情数据的数量
     */
    int count();

}
