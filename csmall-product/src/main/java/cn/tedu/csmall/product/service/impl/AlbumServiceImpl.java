package cn.tedu.csmall.product.service.impl;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.web.ServiceCode;
import cn.tedu.csmall.product.dao.persist.repository.IAlbumRepository;
import cn.tedu.csmall.product.dao.persist.repository.IPictureRepository;
import cn.tedu.csmall.product.dao.persist.repository.ISkuRepository;
import cn.tedu.csmall.product.dao.persist.repository.ISpuRepository;
import cn.tedu.csmall.product.pojo.param.AlbumAddNewParam;
import cn.tedu.csmall.product.pojo.param.AlbumUpdateInfoParam;
import cn.tedu.csmall.product.pojo.entity.Album;
import cn.tedu.csmall.product.pojo.vo.AlbumListItemVO;
import cn.tedu.csmall.product.pojo.vo.AlbumStandardVO;
import cn.tedu.csmall.product.dao.cache.IAlbumRedisRepository;
import cn.tedu.csmall.product.service.IAlbumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AlbumServiceImpl implements IAlbumService {

    @Value("${csmall.crud.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private IAlbumRepository albumRepository;
    @Autowired
    private IPictureRepository pictureRepository;
    @Autowired
    private IAlbumRedisRepository albumRedisRepository;
    @Autowired
    private ISpuRepository spuRepository;
    @Autowired
    private ISkuRepository skuRepository;

    public AlbumServiceImpl() {
        log.debug("创建业务类对象：AlbumServiceImpl");
    }

    @Override
    public void addNew(AlbumAddNewParam albumAddNewParam) {
        log.debug("开始处理【添加相册】的业务，参数：{}", albumAddNewParam);
        // 调用参数对象的getName()得到尝试添加的相册的名称
        String name = albumAddNewParam.getName();
        // 调用Mapper对象的countByName()执行统计查询
        int count = albumRepository.countByName(name);
        log.debug("根据名称【{}】统计数量，结果：{}", name, count);
        // 判断统计结果是否大于0
        if (count > 0) {
            // 是：名称被占用，抛出异常
            String message = "添加相册失败，相册名称已经被占用！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 创建Album对象
        Album album = new Album();
        // 调用BeanUtils.copyProperties(源,目标)将参数对象中的属性复制到Album对象中
        BeanUtils.copyProperties(albumAddNewParam, album);
        // 调用Mapper对象的insert()执行插入相册数据
        log.debug("即将执行插入数据，参数：{}", album);
        int rows = albumRepository.insert(album);
        if (rows != 1) {
            String message = "添加相册失败！服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void delete(Long id) {
        log.debug("开始处理【根据ID删除相册】的业务，参数：{}", id);
        // 调用Mapper对象的getStandardById()执行查询
        AlbumStandardVO album = albumRepository.getStandardById(id);
        log.debug("根据ID={}检查相册数据是否存在，查询结果：{}", id, album);
        // 判断查询结果是否为null
        if (album == null) {
            String message = "删除相册失败，尝试删除的相册数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 检查此相册中是否包含图片
        int pictureCount = pictureRepository.countByAlbum(id);
        if (pictureCount > 0) {
            String message = "删除相册失败，此相册中仍包含图片！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 检查此相册中是否关联SPU
        int spuCount = spuRepository.countByAlbum(id);
        if (spuCount > 0) {
            String message = "删除相册失败，此相册仍关联商品！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 检查此相册中是否关联SKU
        int skuCount = skuRepository.countByAlbum(id);
        if (skuCount > 0) {
            String message = "删除相册失败，此相册仍关联商品！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 调用Mapper对象的deleteById()方法执行删除
        log.debug("即将执行删除，参数：{}", id);
        int rows = albumRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除相册失败，服务器忙，请稍后再次尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }
    }

    @Override
    public void updateInfoById(Long id, AlbumUpdateInfoParam albumUpdateInfoParam) {
        log.debug("开始处理【修改相册详情】的业务，ID：{}，新数据：{}", id, albumUpdateInfoParam);
        // 调用Mapper对象的getStandardById()执行查询
        AlbumStandardVO queryResult = albumRepository.getStandardById(id);
        // 判断查询结果是否为null
        if (queryResult == null) {
            // 是：抛出异常
            String message = "修改相册详情失败，尝试修改的相册数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 调用Mapper对象的countByNameAndNotId()执行统计
        int count = albumRepository.countByNameAndNotId(id, albumUpdateInfoParam.getName());
        // 判断统计结果是否大于0
        if (count > 0) {
            // 是：名称被占用，抛出异常
            String message = "修改相册详情失败，相册名称已经被占用！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        // 创建Album对象
        Album album = new Album();
        // 复制属性，设置ID
        BeanUtils.copyProperties(albumUpdateInfoParam, album);
        album.setId(id);
        // 调用Mapper对象的update()方法执行修改
        int rows = albumRepository.update(album);
        if (rows != 1) {
            String message = "修改相册详情失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }

    @Override
    public AlbumStandardVO getStandardById(Long id) {
        log.debug("开始处理【根据ID查询相册详情】的业务，参数：{}", id);
        AlbumStandardVO queryResult = albumRepository.getStandardById(id);
        // AlbumStandardVO queryResult = albumRedisRepository.get(id);
        if (queryResult == null) {
            // 是：抛出异常
            String message = "查询相册详情失败，相册数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return queryResult;
    }

    @Override
    public PageData<AlbumListItemVO> list(Integer pageNum) {
        log.debug("开始处理【查询相册列表】的业务，页码：{}", pageNum);
        PageData<AlbumListItemVO> pageData = albumRepository.list(pageNum, defaultQueryPageSize);
        return pageData;
    }

    @Override
    public PageData<AlbumListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理【查询相册列表】的业务，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageData<AlbumListItemVO> pageData = albumRepository.list(pageNum, pageSize);
        return pageData;
    }

    @Override
    public void rebuildCache() {
        log.debug("开始处理【重建相册缓存】的业务，无参数");
        // 删除缓存中的相册列表
        albumRedisRepository.deleteList();
        // 从数据库中查询所有相册的列表
        List<AlbumListItemVO> list = albumRepository.list(1, defaultQueryPageSize).getList();
        // 将相册列表写入到缓存中
        albumRedisRepository.save(list);

        // 删除缓存中所有的相册的数据项
        albumRedisRepository.deleteAllItem();
        // 遍历相册列表的数据
        for (AlbumListItemVO albumListItemVO : list) {
            // 获取每个相册数据的ID，并根据此ID从数据库中查询相册的详情
            AlbumStandardVO albumStandardVO
                    = albumRepository.getStandardById(albumListItemVO.getId());
            // 将每一个相册详情写入到缓存中
            albumRedisRepository.save(albumStandardVO);
        }
    }

}
