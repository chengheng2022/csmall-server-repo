package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.Attribute;
import cn.tedu.csmall.product.pojo.vo.AttributeListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeStandardVO;

import java.util.Collection;
import java.util.List;

/**
 * 处理属性数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IAttributeRepository {

    /**
     * 插入属性数据
     *
     * @param attribute 属性数据
     * @return 受影响的行数
     */
    int insert(Attribute attribute);

    /**
     * 批量插入属性数据
     *
     * @param attributeList 若干个属性数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Attribute> attributeList);

    /**
     * 根据id删除属性数据
     *
     * @param id 属性ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除属性
     *
     * @param idList 需要删除的若干个属性的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新属性数据
     *
     * @param attribute 封装了属性的id和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(Attribute attribute);

    /**
     * 统计属性数据的数量
     *
     * @return 属性数据的数量
     */
    int count();

    /**
     * 根据属性模板统计属性数据的数量
     *
     * @param templateId 属性模板ID
     * @return 此属性模板中属性数据的数量
     */
    int countByTemplate(Long templateId);

    /**
     * 根据属性名称和属性模板统计当前表中属性数据的数量
     *
     * @param name       属性名称
     * @param templateId 属性模板ID
     * @return 当前表中匹配名称的属性数据的数量
     */
    int countByNameAndTemplate(String name, Long templateId);

    /**
     * 统计当前表中非此属性id的匹配名称、匹配属性模板的属性数据的数量
     *
     * @param id         当前属性ID
     * @param name       属性名称
     * @param templateId 属性模板ID
     * @return 当前表中非此属性id的匹配名称、匹配属性模板的属性数据的数量
     */
    int countByNameAndTemplateAndNotId(Long id, String name, Long templateId);

    /**
     * 根据id查询属性数据详情
     *
     * @param id 属性ID
     * @return 匹配的属性数据详情，如果没有匹配的数据，则返回null
     */
    AttributeStandardVO getStandardById(Long id);

    /**
     * 根据属性模板查询属性数据列表
     *
     * @param templateId 属性模板ID
     * @param pageNum    页码
     * @param pageSize   每页记录数
     * @return 属性数据列表
     */
    PageData<AttributeListItemVO> listByTemplateId(Long templateId, Integer pageNum, Integer pageSize);

}
