package cn.tedu.csmall.product.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.product.dao.persist.mapper.AttributeTemplateMapper;
import cn.tedu.csmall.product.dao.persist.repository.IAttributeTemplateRepository;
import cn.tedu.csmall.product.pojo.entity.AttributeTemplate;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateListItemVO;
import cn.tedu.csmall.product.pojo.vo.AttributeTemplateStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理属性模板数据的存储库实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class AttributeTemplateRepositoryImpl implements IAttributeTemplateRepository {

    @Autowired
    private AttributeTemplateMapper attributeTemplateMapper;

    public AttributeTemplateRepositoryImpl() {
        log.debug("创建数据访问实现类对象：AttributeTemplateRepositoryImpl");
    }

    @Override
    public int insert(AttributeTemplate attributeTemplate) {
        log.debug("开始执行【插入属性模板】的数据访问，参数：{}", attributeTemplate);
        return attributeTemplateMapper.insert(attributeTemplate);
    }

    @Override
    public int insertBatch(List<AttributeTemplate> attributeTemplateList) {
        log.debug("开始执行【批量插入属性模板】的数据访问，参数：{}", attributeTemplateList);
        return attributeTemplateMapper.insertBatch(attributeTemplateList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除属性模板】的数据访问，参数：{}", id);
        return attributeTemplateMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除属性模板】的数据访问，参数：{}", idList);
        return attributeTemplateMapper.deleteBatchIds(idList);
    }

    @Override
    public int update(AttributeTemplate attributeTemplate) {
        log.debug("开始执行【更新属性模板】的数据访问，参数：{}", attributeTemplate);
        return attributeTemplateMapper.updateById(attributeTemplate);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计属性模板的数量】的数据访问，参数：无");
        return attributeTemplateMapper.selectCount(null);
    }

    @Override
    public int countByName(String name) {
        log.debug("开始执行【统计匹配名称的属性模板的数量】的数据访问，名称：{}", name);
        QueryWrapper<AttributeTemplate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return attributeTemplateMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByNameAndNotId(Long id, String name) {
        log.debug("开始执行【统计匹配名称但不匹配ID的属性模板的数量】的数据访问，ID：{}，名称：{}", id, name);
        QueryWrapper<AttributeTemplate> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name).ne("id", id);
        return attributeTemplateMapper.selectCount(queryWrapper);
    }

    @Override
    public AttributeTemplateStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询属性模板信息】的数据访问，参数：{}", id);
        return attributeTemplateMapper.getStandardById(id);
    }

    @Override
    public PageData<AttributeTemplateListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询属性模板列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<AttributeTemplateListItemVO> list = attributeTemplateMapper.list();
        PageInfo<AttributeTemplateListItemVO> pageInfo = new PageInfo<>(list);
        PageData<AttributeTemplateListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }
}
