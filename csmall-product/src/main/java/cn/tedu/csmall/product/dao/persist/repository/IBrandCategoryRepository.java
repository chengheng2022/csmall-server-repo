package cn.tedu.csmall.product.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.product.pojo.entity.BrandCategory;
import cn.tedu.csmall.product.pojo.vo.BrandCategoryListItemVO;
import cn.tedu.csmall.product.pojo.vo.BrandCategoryStandardVO;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * 处理品牌与类别的关联数据的存储库接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IBrandCategoryRepository {
    /**
     * 插入品牌与类别的关联数据
     *
     * @param brandCategory 品牌与类别的关联数据
     * @return 受影响的行数
     */
    int insert(BrandCategory brandCategory);

    /**
     * 批量插入品牌与类别的关联数据
     *
     * @param brandCategoryList 若干个品牌与类别的关联数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<BrandCategory> brandCategoryList);

    /**
     * 根据id删除品牌与类别的关联数据
     *
     * @param id 品牌与类别的关联数据的ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 批量删除品牌与类别的关联数据
     *
     * @param idList 需要删除的若干个品牌与类别的关联数据的ID
     * @return 受影响的行数
     */
    int deleteByIds(Collection<Long> idList);

    /**
     * 更新品牌与类别的关联数据
     *
     * @param brandCategory 封装了品牌与类别的关联数据的ID和需要更新的新数据的对象
     * @return 受影响的行数
     */
    int update(BrandCategory brandCategory);

    /**
     * 统计品牌与类别的关联数据的数量
     *
     * @return 品牌与类别的关联数据的数量
     */
    int count();

    /**
     * 根据品牌统计关联数据的数量
     *
     * @param brandId 品牌ID
     * @return 此品牌关联的数据的数量
     */
    int countByBrand(Long brandId);

    /**
     * 根据类别统计关联数据的数量
     *
     * @param categoryId 类别ID
     * @return 此类别关联的数据的数量
     */
    int countByCategory(Long categoryId);

    /**
     * 根据品牌与类别统计关联数据的数量
     *
     * @param brandId    品牌ID
     * @param categoryId 类别ID
     * @return 此属性模板关联的数据的数量
     */
    int countByBrandAndCategory(@Param("brandId") Long brandId, @Param("categoryId") Long categoryId);

    /**
     * 根据id查询品牌与类别的关联数据详情
     *
     * @param id 品牌与类别的关联ID
     * @return 匹配的品牌与类别的关联数据详情，如果没有匹配的数据，则返回null
     */
    BrandCategoryStandardVO getStandardById(Long id);

    /**
     * 查询品牌与类别的关联数据列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 品牌与类别的关联数据列表
     */
    PageData<BrandCategoryListItemVO> list(Integer pageNum, Integer pageSize);
}
