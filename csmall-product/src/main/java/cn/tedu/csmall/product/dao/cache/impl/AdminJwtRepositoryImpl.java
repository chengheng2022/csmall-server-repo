package cn.tedu.csmall.product.dao.cache.impl;

import cn.tedu.csmall.commons.consts.JwtCacheConsts;
import cn.tedu.csmall.commons.pojo.po.AdminLoginInfoPO;
import cn.tedu.csmall.product.dao.cache.IAdminJwtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * 处理管理员数据缓存的访问实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Repository
public class AdminJwtRepositoryImpl implements IAdminJwtRepository, JwtCacheConsts {

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Override
    public AdminLoginInfoPO getLoginInfo(String jwt) {
        String key = ADMIN_JWT_PREFIX + jwt;
        Serializable serializable = redisTemplate.opsForValue().get(key);
        AdminLoginInfoPO adminLoginInfoPO = (AdminLoginInfoPO) serializable;
        return adminLoginInfoPO;
    }

}
