package cn.tedu.csmall.passport;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT演示测试类
 */
public class JwtTests {

    String secretKey = "gfd89uiKa89J043tAFrflkji9432kjfdsajm";

    /**
     * 测试：生成JWT
     */
    @Test
    void generate() {
        Map<String, Object> claims = new HashMap<>();
        // claims.put("i", 1);
        claims.put("id", 9527);
        claims.put("username", "spring");
        //claims.put("email", "spring@baidu.com");

        String jwt = Jwts.builder()
                // Header：声明算法与Token类型
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("typ", "JWT")
                // Payload：数据，具体表现为Claims
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + 10 * 24 * 60 * 60 * 1000))
                // Verify Signature：验证签名
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
        System.out.println(jwt);
    }

    /**
     * 测试：解析JWT
     */
    @Test
    void parse() {
        try {
            String jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OTUyNywiZXhwIjoxNjc5MTA4NzI2LCJ1c2VybmFtZSI6InNwcmluZyJ9.uNPat7lLD1miqD9-GvZ2XJTjoNH2L1GsniZK4PDhgEQ";
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwt).getBody();
            Object id = claims.get("id");
            Object username = claims.get("username");
            System.out.println("id = " + id);
            System.out.println("username = " + username);
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

}
