package cn.tedu.csmall.passport.dao.persist.repository;

import cn.tedu.csmall.passport.pojo.entity.AdminRole;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class AdminRoleRepositoryTests {

    @Autowired
    IAdminRoleRepository repository;

    @Test
    void insertBatch() {
        List<AdminRole> adminRoles = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            AdminRole adminRole = new AdminRole();
            adminRole.setAdminId(100L);
            adminRole.setRoleId(i + 0L);
            adminRoles.add(adminRole);
        }

        int rows = repository.insertBatch(adminRoles);
        log.debug("批量插入完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByAdminId() {
        Long adminId = 1L;
        int rows = repository.deleteByAdminId(adminId);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

}
