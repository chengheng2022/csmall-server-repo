package cn.tedu.csmall.passport;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * BCrypt算法演示测试类
 */
public class BCryptTests {

    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    /**
     * 测试：执行编码
     */
    @Test
    void encode() {
        String rawPassword = "123456";
        System.out.println("原文：" + rawPassword);

        for (int i = 0; i < 50; i++) {
            String encodedPassword = passwordEncoder.encode(rawPassword);
            System.out.println("密文：" + encodedPassword);
        }
    }

    /**
     * 测试：验证原文与密文是否匹配
     */
    @Test
    void matches() {
        String rawPassword = "123456";
        System.out.println("原文：" + rawPassword);

        String encodedPassword = "$2a$10$axTarVyFEKGhlkA6qYyZIeY.V7PVC7vcLFkTXFo7K0k46SDzUDVg2";
        System.out.println("密文：" + encodedPassword);

        boolean result = passwordEncoder.matches(rawPassword, encodedPassword);
        System.out.println("验证结果：" + result);
    }

    /**
     * 测试：执行效率 >>> 效率非常低下
     */
    @Test
    void testLoop() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 12; i++) {
            passwordEncoder.encode("");
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

}

//        密文：$2a$10$UFVVutZGQ2tOTW1sLlgQa.WYitxStxE81P0rGfnjI4O8IRFVlhpMS
//        密文：$2a$10$axTarVyFEKGhlkA6QYyZIeY.V7PVC7vcLFkTXFo7K0k46SDzUDVg2
//        密文：$2a$10$uYh7t2.IsYB4JrPfjDxQvuvwPYE.CY17P5ftU19hcQD35X9qIXv4q
//        密文：$2a$10$7QDsNoMhiazd2pcVsyOkPeIDMUZbRfbaRVG7FrlfsfZuQMx61obSi
//        密文：$2a$10$X2QJu6jhuzA6IJOnI/WpSOdIdsGsdF1E7qwyHya6V8yqnS08jv7CS
//        密文：$2a$10$2eNijBY2XapLdt1QdwJW2.853TSKvKrNnEvojkfROFEvyD34wFFQe
//        密文：$2a$10$5FV2/IiWp0HQeb8/9UzDC.WizAleU2X8kSZpEHe8Z6L4bdCgIAkea
//        密文：$2a$10$O0x7IZQqWXqWOv4phbw9BOttBBLpuaTNWCDmWQuNQrQMwGE5jW6Fu
//        密文：$2a$10$WzKE326BoZc6e5HmV84DI.QBh6KuJQg0uYJnuyLWCND4nRQdFORjq
//        密文：$2a$10$.s5KCGf9ksc4xtOBof.6LO8dKGv.bYCtzS5CiSUeWP923KD11VT6a
//        密文：$2a$10$4.Y3.NhSrEs6WI4GNUHLkufQoq5sOHH6DAmMbMkePUKeWVhgUt2O6
//        密文：$2a$10$MiMRy15DaZJcGHtwGfCJCelkgAuJ57lkwsKXG6JPm3p2vvH4A9BcC
//        密文：$2a$10$jSolKJjeyOZ9hosdOpTcgOmkiahtuils1O9XeYsM1TauIG7aQQflO
//        密文：$2a$10$JaCnsnrRarvpmoEy.KixF.PazamGHczh7fLnbesfZJl1n5eE2wwb.
//        密文：$2a$10$5/o.NS7FfdJyqdSnTo.UX.DPkWDWlbgRnZVpfcZvuwmvUHAA7BgfS
//        密文：$2a$10$K5j.neWGS7Qs6m8YY.MfAOir8ducDKmvR3CyhW0PSkW/yqdcEUP3a
//        密文：$2a$10$ir7at6PCLJHX8/gXy0wSEOFzdbHwmht5dREEbQADYXcZW/Na/6/Ia
//        密文：$2a$10$W4TQ.dxkpkfh2gm19ZxdVu2EqZWWWfNZrn8v5b7mzutKryL9f1QY.
//        密文：$2a$10$LjbjUA87EQcZhjgU1In5vOc9B9AQxiykO2tYY6uYvMf.vFElHivHe
//        密文：$2a$10$8IPcda.K7GqPshoswD1DpudnRBviPJqqCTNTH.NKtwIlj1hUZJUjG
//        密文：$2a$10$.Lf3zGCDuI1p8rmSrnnFSe1S1VPp1ZLV6U48joFSiGDFBauO1iWQ2
//        密文：$2a$10$B/bRRUVolnU4mYfUK6FZlOgejSUTs6EXdqFyydrZPFi8VHA8u7QPC
//        密文：$2a$10$PnbJfqaeO9IVEuPnrg2zWu8dHIf1rHgAQdkxKHRjeY/Y//nOTv9Wa
//        密文：$2a$10$yOND7RZ3SwqxBQTRn9e3iO2gke4F88.BoNF18w5B.HmXTZb9I1z5.
//        密文：$2a$10$.GovWgscofRGJP/s/UV2G.bZu2oTmUwfwSquzotUe5k1crskwMvZy
//        密文：$2a$10$OyhOodddqixWxSVNZWUbfujTx7qBPq56ZGz3pxBtRA4IL7SwUd25u
//        密文：$2a$10$cC4QhRzGSJoVAKDlKjw/9Oh0TyKMZQC3jzFGtVR/qxX9rISqJaiIG
//        密文：$2a$10$uuZ.RcWaHOxLQL.s9XqrHOlwkrPJwONLz3oVdX0cP2sn0s8o4Feki
//        密文：$2a$10$ox4rYXG/V9VSSKeMLl1NT.zbhdxFtoUkaXbylFiFtJW7ldvkLNZeG
//        密文：$2a$10$lfZAi1oKNMsrojaHixcGReqns59QCejSgLYkHr2pLzfXA3XEiwiE6
//        密文：$2a$10$xUTjBur0TEoBR81kapbSzOE9o3tnuMKIFFMj7w4lIkLqk6Y9kDLGu
//        密文：$2a$10$uoFxhJtatVAzYi9.84vAou/V/xo74Azw6/lxJzmcua62lxSBRr3vi
//        密文：$2a$10$fNIIHP3klWIgqZcCkHO4ze86Pon3DzEHdtqZ/cL7YV5QV90bYcmyG
//        密文：$2a$10$99TUIktt5Aj8RNuBhZI/tesZ0/noZc6YdWoahcpM2qoht1spmP8ky
//        密文：$2a$10$AtHMBnKsbGNjfYhQbOGe1uB7hN3wb6Q5F7VH5gNIOPRbJVN/u1CMq
//        密文：$2a$10$5c/utDao2qhrxFiZSYq1BuNwn4J4oQMGColjD/c842lfpAvq6/4ou
//        密文：$2a$10$J8c5SmBkcRm201h9W3Ns2e3146LzeSmOicJJMu7pgGrGa6OUkJVMa
//        密文：$2a$10$yYCggK7OjFHG7IWOdYhDNuLBl3dBeMf8ycaMjHjI6D8775zWmyLFK
//        密文：$2a$10$e3.5GL4e.DC5fGCmcSbxi.6vlpooyFm.2QxcxxRSMR6TgjslxqkUq
//        密文：$2a$10$M0PsVWkU1NdTbPrHRC2wEO0Fqd3qnGsoTUqQa9j7oy.OQxEQJXzE6
//        密文：$2a$10$oKCqgCXAer4rGmhG7O9Ti.mhgEd2mvCEaI.gBlTuJ11QEyJWbaBTm
//        密文：$2a$10$GHkLsNekCargEevD/PhB4.MW7O1/y1lQ0oXumF.5uaMbCoXg5E3fO
//        密文：$2a$10$xD3Ka3ux60jtUk2kpgUvo.Tqs9w.WHU18awv8y5XK6Zgcl74hXuTm
//        密文：$2a$10$GXcbjWI/MjxmRg.lWvpEeOo1K9ZWUsTOqFgqyt4or.dpg.IoFXkQy
//        密文：$2a$10$pn0scKf6K.oot4VIDWtBFuDr0i9vsV/KabJrGF1V/Eb3r7u5VgF7q
//        密文：$2a$10$QJCTJlay2XTAPoXm4so2qu/cHAkZPfMMIRFQwxT8Eako7fe8AGtG.
//        密文：$2a$10$O8j4OqfTZXc7qdOcrwOm2eq0HA2.Fc9eE0XJJU9PQN07TAdkwVCZG
//        密文：$2a$10$j/o5yYgjlCWT5yKZsw1x9eHl4WzB3UgJdagTAZ5lnulJk01TRCceO
//        密文：$2a$10$bfcWsmdUw19k0GFuHUqTf.u62Ok8QvadUulIagMCVU39hfe43l3Fi
//        密文：$2a$10$LRRywZ2FvIW8vXgCltg9IuhtIuFt4UV.biTNkBx7SqE2.8lradW7K