package cn.tedu.csmall.passport;

import org.junit.jupiter.api.Test;
import org.springframework.util.DigestUtils;

import java.util.UUID;

/**
 * MD5算法演示测试类
 */
public class Md5Tests {

    /**
     * 测试：执行编码
     */
    @Test
    void encode() {
        for (int i = 0; i < 5; i++) {
            String salt = UUID.randomUUID().toString();
            String rawPassword = "123456";
            String encodedPassword = DigestUtils.md5DigestAsHex(
                    (rawPassword + salt).getBytes());
            System.out.println("盐值：" + salt);
            System.out.println("原文：" + rawPassword);
            System.out.println("密文：" + salt + encodedPassword);
            System.out.println();

            // 8a3a3d36-036f-484c-998d-0365e1c96de57e7ef13c66431cef53dc1b3f4524c16e
            // 514523ad-0ad2-4ea8-9987-f1e16dec36a2e05d14fd50aaccd29225b893104763d1
            // 4cbac65a-0de9-4e26-ba9b-baa17d50c6b45ad8ac8babdb65eafd4b082836ae51f1
            // d84a9aac-8ba9-46c8-b2e0-bd12bf3b93c3406a195fe1cd78dd6a72d1eb400083be
        }
    }

    /**
     * 测试：执行验证原文与密文是否匹配
     */
    @Test
    void matches() {
        String rawPassword = "123456";
        String dbPassword = "4cbac65b-0de9-4e26-ba9b-baa17d50c6b45ad8ac8babdb65eafd4b082836ae51f1";

        String salt = dbPassword.substring(0, 36);
        String encodedPassword = DigestUtils.md5DigestAsHex(
                (rawPassword + salt).getBytes());

        boolean result = dbPassword.equals(salt + encodedPassword);
        System.out.println("盐值：" + salt);
        System.out.println("原文：" + rawPassword);
        System.out.println("密文（数据库）：" + dbPassword);
        System.out.println("验证结果：" + result);
    }

    /**
     * 测试：多重加密
     */
    @Test
    void multiEncode() {
        String s = "123456";
        System.out.println("原文：" + s);
        for (int i = 0; i < 1000000; i++) {
            s = DigestUtils.md5DigestAsHex(s.getBytes());
        }
        System.out.println("密文：" + s);
    }

    /**
     * 测试：执行效率 >>> 效率较高
     */
    @Test
    void testLoop() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 3000000; i++) {
            DigestUtils.md5DigestAsHex("".getBytes());
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

}
