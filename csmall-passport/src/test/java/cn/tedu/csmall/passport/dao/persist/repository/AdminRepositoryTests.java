package cn.tedu.csmall.passport.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.passport.pojo.entity.Admin;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@SpringBootTest
public class AdminRepositoryTests {

    @Autowired
    IAdminRepository repository;

    @Test
    void insert() {
        Admin admin = new Admin();
        admin.setUsername("wangkejing001");
        admin.setPassword("123456");
        admin.setPhone("13800138001");
        admin.setEmail("wangkejing001@baidu.com");

        log.debug("插入数据之前，参数：{}", admin);
        int rows = repository.insert(admin);
        log.debug("插入数据完成，受影响的行数：{}", rows);
        log.debug("插入数据之后，参数：{}", admin);
    }

    @Test
    void insertBatch() {
        List<Admin> admins = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            Admin admin = new Admin();
            admin.setUsername("test-admin-" + i);
            admins.add(admin);
        }

        int rows = repository.insertBatch(admins);
        log.debug("批量插入完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteById() {
        Long id = 1L;
        int rows = repository.deleteById(id);
        log.debug("删除完成，受影响的行数：{}", rows);
    }

    @Test
    void deleteByIds() {
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(3L);
        idList.add(5L);
        int rows = repository.deleteByIds(idList);
        log.debug("批量删除完成，受影响的行数：{}", rows);
    }

    @Test
    void update() {
        Admin admin = new Admin();
        admin.setId(1L);
        admin.setNickname("新-测试数据001");

        int rows = repository.updateById(admin);
        log.debug("更新完成，受影响的行数：{}", rows);
    }

    @Test
    void count() {
        int count = repository.count();
        log.debug("统计完成，表中的数据的数量：{}", count);
    }

    @Test
    void countByUsername() {
        String username = "wangkejing";
        int count = repository.countByUsername(username);
        log.debug("根据用户名【{}】统计管理员账号的数量：{}", username, count);
    }

    @Test
    void countByPhone() {
        String phone = "13800138001";
        int count = repository.countByPhone(phone);
        log.debug("根据手机号码【{}】统计管理员账号的数量：{}", phone, count);
    }

    @Test
    void countByEmail() {
        String email = "wangkejing@baidu.com";
        int count = repository.countByEmail(email);
        log.debug("根据电子邮箱【{}】统计管理员账号的数量：{}", email, count);
    }

    @Test
    void getStandardById() {
        Long id = 1L;
        Object queryResult = repository.getStandardById(id);
        log.debug("根据id【{}】查询数据详情完成，查询结果：{}", id, queryResult);
    }

    @Test
    void getLoginInfoByUsername() {
        String username = "root";
        Object queryResult = repository.getLoginInfoByUsername(username);
        log.debug("根据用户名【{}】查询数据详情完成，查询结果：{}", username, queryResult);
    }

    @Test
    void list() {
        Integer pageNum = 1;
        Integer pageSize = 5;
        PageData<?> pageData = repository.list(pageNum, pageSize);
        List<?> list = pageData.getList();
        log.debug("查询列表完成，结果：{}", pageData);
        for (Object item : list) {
            log.debug("列表项：{}", item);
        }
    }

}
