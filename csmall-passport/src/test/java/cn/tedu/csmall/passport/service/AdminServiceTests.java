package cn.tedu.csmall.passport.service;

import cn.tedu.csmall.commons.ex.ServiceException;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.passport.pojo.param.AdminAddNewParam;
import cn.tedu.csmall.passport.pojo.param.AdminLoginInfoParam;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
public class AdminServiceTests {

    @Autowired
    IAdminService service;

    @Test
    void login() {
        AdminLoginInfoParam adminLoginInfoParam = new AdminLoginInfoParam();
        adminLoginInfoParam.setUsername("liucangsong");
        adminLoginInfoParam.setPassword("123456");

        try {
            Object loginInfo = service.login(adminLoginInfoParam, null, null);
            log.debug("登录成功，JWT：{}", loginInfo);
        } catch (Throwable e) {
            // 由于不确定Spring Security会抛出什么类型的异常
            // 所以，捕获的是Throwable
            // 并且，在处理时，应该打印信息，以了解什么情况下会出现哪种异常
            e.printStackTrace();
        }
    }

    @Test
    void addNew() {
        AdminAddNewParam adminAddNewParam = new AdminAddNewParam();
        adminAddNewParam.setUsername("管理员100");
        adminAddNewParam.setPassword("123456");
        adminAddNewParam.setPhone("13900139100");
        adminAddNewParam.setEmail("13900139100@baidu.com");
        adminAddNewParam.setRoleIds(new Long[]{2L, 5L});

        try {
            service.addNew(adminAddNewParam);
            log.debug("添加数据完成！");
        } catch (ServiceException e) {
            log.debug(e.getMessage());
        }
    }

    @Test
    void delete() {
        Long id = 7L;

        try {
            service.delete(id);
            log.debug("删除管理员完成！");
        } catch (ServiceException e) {
            log.debug("删除管理员失败！具体原因请参见日志！");
        }
    }

    @Test
    void setEnable() {
        Long id = 1L;

        try {
            service.setEnable(id);
            log.debug("启用管理员完成！");
        } catch (ServiceException e) {
            log.debug("启用管理员失败！具体原因请参见日志！");
        }
    }

    @Test
    void setDisable() {
        Long id = 1L;

        try {
            service.setDisable(id);
            log.debug("禁用管理员完成！");
        } catch (ServiceException e) {
            log.debug("禁用管理员失败！具体原因请参见日志！");
        }
    }

    @Test
    void list() {
        try {
            Integer pageNum = 1;
            Integer pageSize = 5;
            PageData<?> pageData = service.list(pageNum, pageSize);
            List<?> list = pageData.getList();
            log.debug("查询列表完成，结果：{}", pageData);
            for (Object item : list) {
                log.debug("列表项：{}", item);
            }
        } catch (ServiceException e) {
            log.debug("捕获到异常，其中的消息：{}", e.getMessage());
        }
    }
}
