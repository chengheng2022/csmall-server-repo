package cn.tedu.csmall.passport.dao.cache.repository;

import cn.tedu.csmall.commons.pojo.po.AdminLoginInfoPO;

/**
 * 处理管理员数据缓存的访问接口
 */
public interface IAdminJwtRepository {

    /**
     * 保存管理员的登录信息
     *
     * @param jwt              当前管理员的JWT
     * @param adminLoginInfoPO 管理员的登录信息
     * @param jwtDuration      管理员的JWT有效时长
     */
    void saveLoginInfo(String jwt, AdminLoginInfoPO adminLoginInfoPO, long jwtDuration);

    /**
     * 根据管理员的JWT获取登录信息
     *
     * @param jwt 当前管理员的JWT
     * @return 管理员的登录信息，如果没有匹配的数据，将返回null
     */
    AdminLoginInfoPO getLoginInfo(String jwt);

    /**
     * 删除管理员登录信息
     *
     * @param jwt 当前管理员的JWT
     * @return 是否成功删除
     */
    Boolean delete(String jwt);

}
