package cn.tedu.csmall.passport.dao.cache.repository.impl;

import cn.tedu.csmall.commons.consts.JwtCacheConsts;
import cn.tedu.csmall.commons.pojo.po.AdminLoginInfoPO;
import cn.tedu.csmall.passport.dao.cache.repository.IAdminJwtRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

@Repository
public class AdminJwtRepositoryImpl implements IAdminJwtRepository, JwtCacheConsts {

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @Override
    public void saveLoginInfo(String jwt, AdminLoginInfoPO adminLoginInfoPO, long jwtDuration) {
        String key = ADMIN_JWT_PREFIX + jwt;
        redisTemplate.opsForValue().set(key, adminLoginInfoPO, jwtDuration, TimeUnit.MILLISECONDS);
    }

    @Override
    public AdminLoginInfoPO getLoginInfo(String jwt) {
        String key = ADMIN_JWT_PREFIX + jwt;
        Serializable serializable = redisTemplate.opsForValue().get(key);
        AdminLoginInfoPO adminLoginInfoPO = (AdminLoginInfoPO) serializable;
        return adminLoginInfoPO;
    }

    @Override
    public Boolean delete(String jwt) {
        String key = ADMIN_JWT_PREFIX + jwt;
        return redisTemplate.delete(key);
    }

}
