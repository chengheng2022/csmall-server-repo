package cn.tedu.csmall.passport.service;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.passport.pojo.param.AdminAddNewParam;
import cn.tedu.csmall.passport.pojo.param.AdminLoginInfoParam;
import cn.tedu.csmall.passport.pojo.vo.AdminListItemVO;
import cn.tedu.csmall.passport.pojo.dto.AdminLoginInfoDTO;
import org.springframework.transaction.annotation.Transactional;

/**
 * 处理管理员数据的业务接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Transactional
public interface IAdminService {

    /**
     * 类别“是否启用”的状态文本
     */
    String ENABLE_TEXT[] = {"禁用", "启用"};

    /**
     * 管理员登录
     *
     * @param adminLoginInfoParam 封装了用户名、密码等相关信息的对象
     * @param remoteAddr          来访IP地址
     * @param userAgent           浏览器信息
     * @return 此管理员登录后得到的JWT数据
     */
    AdminLoginInfoDTO login(AdminLoginInfoParam adminLoginInfoParam, String remoteAddr, String userAgent);

    /**
     * 管理员退出登录
     *
     * @param jwt 当前已经登录的管理员的JWT
     */
    void logout(String jwt);

    /**
     * 添加管理员
     *
     * @param adminAddNewParam 管理员数据
     */
    void addNew(AdminAddNewParam adminAddNewParam);

    /**
     * 删除管理员
     *
     * @param id 管理员ID
     */
    void delete(Long id);

    /**
     * 启用管理员
     *
     * @param id 管理员ID
     */
    void setEnable(Long id);

    /**
     * 禁用管理员
     *
     * @param id 管理员ID
     */
    void setDisable(Long id);

    /**
     * 查询管理员列表，将使用默认的每页记录数
     *
     * @param pageNum 页码
     * @return 管理员列表
     */
    PageData<AdminListItemVO> list(Integer pageNum);

    /**
     * 查询管理员列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 管理员列表
     */
    PageData<AdminListItemVO> list(Integer pageNum, Integer pageSize);

}
