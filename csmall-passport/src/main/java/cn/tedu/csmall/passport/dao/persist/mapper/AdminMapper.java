package cn.tedu.csmall.passport.dao.persist.mapper;

import cn.tedu.csmall.passport.pojo.vo.AdminLoginInfoVO;
import cn.tedu.csmall.passport.pojo.entity.Admin;
import cn.tedu.csmall.passport.pojo.vo.AdminListItemVO;
import cn.tedu.csmall.passport.pojo.vo.AdminStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理管理员数据的Mapper接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {

    /**
     * 批量插入管理员数据
     *
     * @param adminList 若干个管理员数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<Admin> adminList);

    /**
     * 统计管理员数据的数量
     *
     * @return 管理员数据的数量
     */
    int count();

    /**
     * 根据用户名统计管理员数据的数量
     *
     * @param username 用户名
     * @return 匹配用户名的管理员数据的数据
     */
    int countByUsername(String username);

    /**
     * 根据手机号码统计管理员数据的数量
     *
     * @param phone 手机号码
     * @return 匹配手机号码的管理员数据的数据
     */
    int countByPhone(String phone);

    /**
     * 根据电子邮箱统计管理员数据的数量
     *
     * @param email 电子邮箱
     * @return 匹配电子邮箱的管理员数据的数据
     */
    int countByEmail(String email);

    /**
     * 根据管理员id查询管理员数据详情
     *
     * @param id 管理员id
     * @return 匹配的管理员数据详情，如果没有匹配的数据，则返回null
     */
    AdminStandardVO getStandardById(Long id);

    /**
     * 根据用户名查询管理员的登录信息
     *
     * @param username 用户名
     * @return 匹配的管理员的登录信息，如果没有匹配的数据，则返回null
     */
    AdminLoginInfoVO getLoginInfoByUsername(String username);

    /**
     * 查询管理员数据列表
     *
     * @return 管理员数据列表
     */
    List<AdminListItemVO> list();

}
