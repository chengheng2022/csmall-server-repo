package cn.tedu.csmall.passport.dao.persist.repository;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.passport.pojo.vo.RoleListItemVO;

/**
 * 处理角色数据的数据访问接口
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
public interface IRoleRepository {

    /**
     * 查询角色列表
     *
     * @param pageNum  页码
     * @param pageSize 每页记录数
     * @return 角色列表
     */
    PageData<RoleListItemVO> list(Integer pageNum, Integer pageSize);

}
