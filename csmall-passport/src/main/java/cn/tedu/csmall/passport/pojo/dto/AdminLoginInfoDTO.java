package cn.tedu.csmall.passport.pojo.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 管理员登录信息的DTO
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Data
@Accessors(chain = true)
public class AdminLoginInfoDTO implements Serializable {

    /**
     * 管理员ID
     */
    private Long id;

    /**
     * 管理员用户名
     */
    private String username;

    /**
     * 管理员头像
     */
    private String avatar;

    /**
     * 管理员用于认证的Token
     */
    private String token;

    /**
     * 管理员的Token的有效截止时间
     */
    private Date tokenExpiration;

}
