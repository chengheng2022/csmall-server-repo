package cn.tedu.csmall.passport.dao.persist.repository;

import cn.tedu.csmall.passport.pojo.entity.AdminRole;

import java.util.List;

public interface IAdminRoleRepository {

    /**
     * 批量插入管理员与角色的关联数据
     *
     * @param adminRoleList 若干个管理员与角色的关联数据的集合
     * @return 受影响的行数
     */
    int insertBatch(List<AdminRole> adminRoleList);

    /**
     * 根据管理员id删除管理员与角色的关联数据
     *
     * @param adminId 管理员id
     * @return 受影响的行数
     */
    int deleteByAdminId(Long adminId);
}
