package cn.tedu.csmall.passport.controller;

import cn.tedu.csmall.commons.consts.HttpConsts;
import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.web.JsonResult;
import cn.tedu.csmall.passport.pojo.param.AdminAddNewParam;
import cn.tedu.csmall.passport.pojo.param.AdminLoginInfoParam;
import cn.tedu.csmall.passport.pojo.vo.AdminListItemVO;
import cn.tedu.csmall.passport.pojo.dto.AdminLoginInfoDTO;
import cn.tedu.csmall.passport.service.IAdminService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 处理管理员相关请求的控制器
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@RestController
@RequestMapping("/admins")
@Validated
@Api(tags = "01. 管理员管理模块")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    public AdminController() {
        log.debug("创建控制器类对象：AdminController");
    }

    // http://localhost:9081/admins/login
    @PostMapping("/login")
    @ApiOperation("管理员登录")
    @ApiOperationSupport(order = 10)
    public JsonResult<AdminLoginInfoDTO> login(AdminLoginInfoParam adminLoginInfoParam, @ApiIgnore HttpServletRequest request) {
        log.debug("开始处理【管理员登录】的请求，参数：{}", adminLoginInfoParam);
        String remoteAddr = request.getRemoteAddr();
        String userAgent = request.getHeader(HttpConsts.HEADER_USER_AGENT);
        AdminLoginInfoDTO loginInfo = adminService.login(adminLoginInfoParam, remoteAddr, userAgent);
        log.debug("即将响应：{}", loginInfo);
        return JsonResult.ok(loginInfo);
    }

    // http://localhost:9081/admins/logout
    @PostMapping("/logout")
    @ApiOperation("管理员退出登录")
    @ApiOperationSupport(order = 11)
    public JsonResult<Void> logout(@RequestHeader(HttpConsts.HEADER_AUTHORIZATION) String jwt) {
        log.debug("开始处理【管理员退出登录】的请求，参数：{}", jwt);
        adminService.logout(jwt);
        return JsonResult.ok();
    }

    // http://localhost:9081/admins/add-new
    @PostMapping("/add-new")
    @PreAuthorize("hasAuthority('/ams/admin/add-new')")
    @ApiOperation("添加管理员")
    @ApiOperationSupport(order = 100)
    public JsonResult<Void> addNew(@Valid AdminAddNewParam adminAddNewParam) {
        log.debug("开始处理【添加管理员】的请求，参数：{}", adminAddNewParam);
        adminService.addNew(adminAddNewParam);
        return JsonResult.ok();
    }

    // http://localhost:9081/admins/9527/delete
    @PostMapping("/{id:[0-9]+}/delete")
    @PreAuthorize("hasAuthority('/ams/admin/delete')")
    @ApiOperation("根据ID删除管理员")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", required = true, dataType = "long")
    })
    public JsonResult<Void> delete(@PathVariable Long id) {
        log.debug("开始处理【根据ID删除管理员】的请求，参数：{}", id);
        adminService.delete(id);
        return JsonResult.ok();
    }

    // http://localhost:9081/admins/9527/enable
    @PostMapping("/{id:[0-9]+}/enable")
    @PreAuthorize("hasAuthority('/ams/admin/update')")
    @ApiOperation("启用管理员")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", required = true, dataType = "long")
    })
    public JsonResult<Void> setEnable(@PathVariable Long id) {
        log.debug("开始处理【启用管理员】的请求，参数：{}", id);
        adminService.setEnable(id);
        return JsonResult.ok();
    }

    // http://localhost:9081/admins/9527/disable
    @PostMapping("/{id:[0-9]+}/disable")
    @PreAuthorize("hasAuthority('/ams/admin/update')")
    @ApiOperation("禁用管理员")
    @ApiOperationSupport(order = 311)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "管理员ID", required = true, dataType = "long")
    })
    public JsonResult<Void> setDisable(@PathVariable Long id) {
        log.debug("开始处理【禁用管理员】的请求，参数：{}", id);
        adminService.setDisable(id);
        return JsonResult.ok();
    }

    // http://localhost:9081/admins
    @GetMapping("")
    @PreAuthorize("hasAuthority('/ams/admin/read')")
    @ApiOperation("查询管理员列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", dataType = "long")
    })
    public JsonResult<PageData<AdminListItemVO>> list(Integer page) {
        log.debug("开始处理【查询管理员列表】的请求，页码：{}", page);
        Integer pageNum = page == null ? 1 : page;
        PageData<AdminListItemVO> pageData = adminService.list(pageNum);
        return JsonResult.ok(pageData);
    }

}