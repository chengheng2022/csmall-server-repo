package cn.tedu.csmall.passport.dao.persist.repository.impl;

import cn.tedu.csmall.passport.dao.persist.mapper.AdminRoleMapper;
import cn.tedu.csmall.passport.dao.persist.repository.IAdminRoleRepository;
import cn.tedu.csmall.passport.pojo.entity.AdminRole;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理管理员与角色关联数据的数据访问实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class AdminRoleRepositoryImpl implements IAdminRoleRepository {

    @Autowired
    private AdminRoleMapper adminRoleMapper;

    public AdminRoleRepositoryImpl() {
        log.debug("创建数据访问实现类对象：AdminRoleRepositoryImpl");
    }

    @Override
    public int insertBatch(List<AdminRole> adminRoleList) {
        log.debug("开始执行【批量插入管理员与角色的关联数据】的数据访问，参数：{}", adminRoleList);
        return adminRoleMapper.insertBatch(adminRoleList);
    }

    @Override
    public int deleteByAdminId(Long adminId) {
        log.debug("开始执行【根据管理员ID删除管理员与角色的关联数据】的数据访问，参数：{}", adminId);
        QueryWrapper<AdminRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("admin_id", adminId);
        return adminRoleMapper.delete(queryWrapper);
    }
}
