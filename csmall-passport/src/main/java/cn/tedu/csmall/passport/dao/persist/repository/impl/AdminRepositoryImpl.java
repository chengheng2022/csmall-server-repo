package cn.tedu.csmall.passport.dao.persist.repository.impl;

import cn.tedu.csmall.commons.pojo.vo.PageData;
import cn.tedu.csmall.commons.util.PageInfoToPageDataConverter;
import cn.tedu.csmall.passport.dao.persist.mapper.AdminMapper;
import cn.tedu.csmall.passport.dao.persist.repository.IAdminRepository;
import cn.tedu.csmall.passport.pojo.entity.Admin;
import cn.tedu.csmall.passport.pojo.vo.AdminListItemVO;
import cn.tedu.csmall.passport.pojo.vo.AdminLoginInfoVO;
import cn.tedu.csmall.passport.pojo.vo.AdminStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * 处理管理员数据的数据访问实现类
 *
 * @author java@tedu.cn
 * @version 0.0.1
 */
@Slf4j
@Repository
public class AdminRepositoryImpl implements IAdminRepository {

    @Autowired
    private AdminMapper adminMapper;

    public AdminRepositoryImpl() {
        log.debug("创建数据访问实现类对象：AdminRepositoryImpl");
    }

    @Override
    public int insert(Admin admin) {
        log.debug("开始执行【插入管理员】的数据访问，参数：{}", admin);
        return adminMapper.insert(admin);
    }

    @Override
    public int insertBatch(List<Admin> adminList) {
        log.debug("开始执行【批量插入管理员】的数据访问，参数：{}", adminList);
        return adminMapper.insertBatch(adminList);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除管理员】的数据访问，参数：{}", id);
        return adminMapper.deleteById(id);
    }

    @Override
    public int deleteByIds(Collection<Long> idList) {
        log.debug("开始执行【批量删除管理员】的数据访问，参数：{}", idList);
        return adminMapper.deleteBatchIds(idList);
    }

    @Override
    public int updateById(Admin admin) {
        log.debug("开始执行【更新管理员】的数据访问，参数：{}", admin);
        return adminMapper.updateById(admin);
    }

    @Override
    public int count() {
        log.debug("开始执行【统计管理员的数量】的数据访问，参数：无");
        return adminMapper.selectCount(null);
    }

    @Override
    public int countByUsername(String username) {
        log.debug("开始执行【根据用户名统计管理员的数量】的数据访问，参数：{}", username);
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        return adminMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByPhone(String phone) {
        log.debug("开始执行【根据手机号码统计管理员的数量】的数据访问，参数：{}", phone);
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("phone", phone);
        return adminMapper.selectCount(queryWrapper);
    }

    @Override
    public int countByEmail(String email) {
        log.debug("开始执行【根据电子邮箱统计管理员的数量】的数据访问，参数：{}", email);
        QueryWrapper<Admin> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", email);
        return adminMapper.selectCount(queryWrapper);
    }

    @Override
    public AdminStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询管理员详情】的数据访问，参数：{}", id);
        return adminMapper.getStandardById(id);
    }

    @Override
    public AdminLoginInfoVO getLoginInfoByUsername(String username) {
        log.debug("开始执行【根据用户名查询管理员登录信息】的数据访问，参数：{}", username);
        return adminMapper.getLoginInfoByUsername(username);
    }

    @Override
    public PageData<AdminListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询管理员列表】的数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<AdminListItemVO> list = adminMapper.list();
        PageInfo<AdminListItemVO> pageInfo = new PageInfo<>(list);
        PageData<AdminListItemVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

}
